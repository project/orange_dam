## Overview

The _Orange DAM_ Drupal module provides the foundation for integration with Orange Logic's [Orange DAM](https://www.orangelogic.com/products/digital-asset-management-system) and synchronization of Orange DAM data to Drupal. It supplies the generic tooling and API integration required for an Orange DAM integration, while also providing enough flexibility for detailed field mapping.

The module does the following:

- Monitors Orange DAM for changes.
- Queues the changes it finds.
- Prepares the updated data for migrations.
- Allows the user to map fields in custom migrations.
- Manages the running of the above processes via cron or via custom Drush commands.

For further details, see the "Key Functionality" and "Usage" sections below.

### Future Development

The Orange DAM API is extensive and this module just scratches the surface of available functionality. Some _ideas_ about where this module could go next include:

- Enable upload of content to Orange DAM from Drupal.
- Access file metadata.
- Document support.
- Create links between documents.
- Integration with Figma asset browser.
- Integration with Adobe Lightroom.
- Integration with Adobe InDesign.
- PDF conversion.
- Extract waveforms from audio content.

_NOTE:_ This module provides the foundation for a custom integration with Orange DAM. Custom development work will be needed to create a data model in Drupal, build and run the migrations that map your data, as well as integrate with the events this module offers to allow for additional customization of the integration.

To further development of this module via sponsorship or to inquire about a custom integration, [contact Chromatic](https://chromatichq.com/contact-us/)!

### Key Functionality

The module offers the ability to:

- Build upon the API integration using the [orange-dam-php library](https://github.com/ChromaticHQ/orange-dam-php) and the [Symfony services](https://git.drupalcode.org/project/orange_dam/-/blob/1.x/orange_dam.services.yml) it provides.
- Update a single item by System Identifier.
- Update all content of a given type.
- Update all known content.
- Update all content changed since the last check for changes.
- Update all content that changed since a given time.
- Update all keywords.
- Update keywords changed since the last check for changes.
- Update keywords changed since a given time.
- Update all keywords of a given tag type.
- Control the content types retrieved.
- Control the keyword types retrieved.
- List all available API fields.
- Retrieve asset URLs for a given asset format by Record ID.
- Retrieve asset streams for watermarked assets.
- Process content deletions.
- Process keyword deletions.
- Display recently migrated content using time and ID-based filters.
- Display queue status summary.
- [Duplicate item detection](#duplicate-item-detection) when the item type changes in Orange Logic but retains the same ID.
- Use custom migration process plugins.
- Use a fully customized/agnostic approach to data mapping via the [Migrate](https://www.drupal.org/project/migrate) module.
- Customize functionality via [Events](https://api.drupal.org/api/drupal/core%21core.api.php/group/events/10).

## Usage

### Drush Commands

To see all Orange DAM commands run the following:

```shell
drush list --filter=orange-dam
```

Each command provides additional documentation on how it should be used.

```shell
drush orange-dam:example-command -h
```

### Debugging/Logging

There are several log channels that Orange Logic syncing activity data is logged to:

- `orange_dam.migration` - Migration script activity and data issues.
- `orange_dam.api` - API request information and errors.
- `orange_dam.queue` - Queueing events and related problems.

There are many ways to view the logs. They can be viewed in the admin UI (`/admin/reports/dblog`) or via Drush.

For example:

```shell
drush watchdog:show --type=orange_dam.migration
```

In addition, as of release 2.0 of this module, we have provided means for you to customize your own debugging and logging at a much more granular level.

1. Each response object returned from the API now includes methods for returning the exact request structure and request parameters that were sent to the API.
2. This is only useful if your code has access to the response object. Therefore, this module dispatches an OrangeDamApiResponseReceived event which your code can subscribe to. Within the event is the response object (and therefore also access to the response request and data), so your subscriber can now, if desired, log information in a highly customizable way about the requests that are being made, not just the data returned.

### Queues & Migrations

The Orange DAM API is used to retrieve items using a variety of methods and filters that are often triggered via one of the many Drush commands provided by this module. The data about the items returned in the API request are sorted into different queues based upon the `CoreField.DocSubType` property in the API response.

The state of the queues can be seen using the `drush orange-dam:migration-queue-status` command.

```shell
$ drush orange-dam:sync-status
...
------------------------------------ ------------
  Queue Name                           Item Count
 ------------------------------------ ------------
 Orange DAM - foo queue                    5
 Orange DAM - bar queue                    197
 ...
```

Migrations are then used to extract the data from each queue to create or update entities within Drupal.

_It is strongly encouraged to use the migration commands provided by this module to ensure that all of the needed operations are performed._

```shell
$ drush orange-dam:run-migration-queues
```

## Setup

This module provides a foundation for a Orange DAM integration, but due to the custom nature of the needs of an integration, it intentionally leaves many things for implementation by each site. However, it does attempt to provide functionality and customizable configuration of that logic whenever possible.

### Configuration

The [`orange_dam.settings`](./config/schema/orange_dam.schema.yml) configuration object is where the primary configuration can be found/managed.

[Sensible defaults](./config/install/orange_dam.settings.yml) are set when possible.

Example configuration:

```yaml
all_fields:
  - CoreField.DocSubType
  - CoreField.Title
  - Corefield.content
  - SystemIdentifier
tag_types:
  - Color
  - Shapes
content_types:
  - Photograph: { }
  - Book:
    use_datatable: true
    asset_format: TR1
  - File Cabinet:
    asset_format: TRX
  - Scanned Page:
    asset_format: TR20
    asset_format_watermarked: TR20_WATERMARKED
base_path: 'https://example.orangelogic.com'
client_id: XXX
query_string: UseSession=ABC123
client_secret: XXX
system_identifier_pattern: '/^EX[A-Z|\d|_]+$/'
api_items_per_page_keywords: 1000
api_items_per_page_search: 300
api_request_timeout: 30
check_for_duplicates: true
api_max_retry_attempts: 5
```

### Events

Custom events are provided to allow for additional customization of key functionality that may vary site by site.

- [OrangeDamApiRequestEvents::ITEM_DATA](./src/Event/OrangeDamApiRequestEvents.php) - Alter request parameters for a single item.
- [OrangeDamApiRequestEvents::UPDATED_ITEMS](./src/Event/OrangeDamApiRequestEvents.php) - Alter request parameters for updated items.
- [OrangeDamDuplicateItemEvents::DELETED](./src/Event/OrangeDamDuplicateItemEvents.php) - React to a deleted duplicate item.
- [OrangeDamDuplicateItemEvents::DETECTED](./src/Event/OrangeDamDuplicateItemEvents.php) - Detect duplicate items.

### Authentication

This module assumes the usage of OAuth authentication with Orange DAM. Configuring that is outside the scope of this module, but once it is configured adding the needed credentials to this module.

### API Credentials

Add the following to the `orange_dam.settings` configuration:

#### Base Path

```yaml
base_path: https://example.orangelogic.com
```

#### Query String

Optionally configure additional parameters as needed:

```yaml
query_string: 'UseSession=<YOUR ORANGE DAM SESSION ID>
```

#### Client ID & Client Secret

The following two variables should likely not be tracked in the repo. They can either be set directly in a local settings file:

```php
$config['orange_dam.settings']['client_id'] = '<YOUR ORANGE DAM CLIENT ID>';
$config['orange_dam.settings']['client_secret'] = '<YOUR ORANGE DAM CLIENT SECRET>';
```

Or set using the following environment variables:

```shell
ORANGE_DAM_CLIENT_ID=<YOUR ORANGE DAM CLIENT ID>
ORANGE_DAM_CLIENT_SECRET=<YOUR ORANGE DAM SECRET>
```

Add the following to the `settings.php` file if environment variables are used:

```php
if ($orangeDamClientId = getenv('ORANGE_DAM_CLIENT_ID')) {
  $config['orange_dam.settings']['client_id'] = $orangeDamClientId;
}
if ($orangeDamClientSecret = getenv('ORANGE_DAM_CLIENT_SECRET')) {
  $config['orange_dam.settings']['client_secret'] = $orangeDamClientSecret;
}
```

### Field Configuration

The complete list of fields that your Orange DAM provides and that should be retrieved for import via the migrations should be provided in configuration.

Add the following to the `orange_dam.settings` configuration:

#### Base Fields

The `base_fields` value represents the essential fields that are needed to represent items that need to be updated. This is likely being deprecated soon as it was only used in an initial prototype of this module.

```yaml
base_fields:
  - Title
  - SystemIdentifier
  - CoreField.DocSubType
```

#### All Fields

The `all_fields` value represents the complete list of fields that should be retrieved for all content types when making API requests using the [Search API endpoint](https://github.com/ChromaticHQ/orange-dam-php/blob/1.x/src/Endpoints/Search.php).

```yaml
all_fields:
  - Title
  - SystemIdentifier
  - CoreField.DocSubType
  - ExampleField
  - AnotherCustomField
```

### Content Type Configuration

This section allows you to set up the Orange DAM content types you wish to access. The configuration consists of the name of the content type as well as the ability to configure the manner in which the data for that content type is retrieved.

If the content type name includes spaces, wrap the content type in single quotes.

The content type name should include either an empty configuration dictionary `{ }` or, if you need to use the DataTable API in order to retrieve more data for the content type, then set the `use_datatable` configuration key to true.

The DataTable API allows for extra metadata about relationships, such as the RecordIDs of related authority lists and other objects to supplement the core data of the content type. Setting `use_datatable` to true will incur an extra API request, so use it only if you genuinely need it. To see how the use of this property changes the retrieved data, queue the same content item with `use_datatable` set to true and also without and see if you require the extra data. Again, we recommend turning it off for performance reasons and only enabling it if you need the extra relational metadata.

It's also worth noting that content items will include all fields configured in the `all_fields` property, whether these fields are used or not on particular content types. There is no way currently to customize per content type which fields show up in your content type's data. All content types will have the same fields in the retrieved object and unused fields will be empty.

### Migrations

A migration should be created for each item type in Orange DAM that should be migrated into Drupal.

This module uses queues to store the data returned from the API for the migrations to consume. It uses the [migrate_source_queue module](https://www.drupal.org/project/migrate_source_queue) to accomplish this. Thus requires all migrations to uses queues as their source.

The queues used as the source for these migrations are [registered dynamically](./src/Plugin/Derivative/OrangeDamMigrationQueues.php) by searching for all migrations tagged with `orange_dam`. The `drush orange-dam:migration-queue-status` command will assist you with seeing the queues and the migrations associated with them.

#### Example Migration

When creating the migrations the following should be considered:

- The migration `id` should be based on an snake-cased form of the `CoreField.DocSubType` property of the Orange Logic API response and prefixed with `orange_dam_`. All non-word characters, including dashes, will be [replaced](https://git.drupalcode.org/project/orange_dam/-/blob/1.x/src/OrangeDamMigrationDataManager.php#:~:text=function%20buildMigrationName) with an underscore.
  - Examples:
    - If the `CoreField.DocSubType` is "Audio Container", the migration ID will be `orange_dam_audio_container`.
    - If the `CoreField.DocSubType` is "Document+Nav/Sub Folder", the migration ID will be `orange_dam_document_nav_sub_folder`.
  - The migration configuration file name must match the migration `id`. So, if your `id` is `orange_dam_audio_container`, your migration file should be `migrate_plus.migration.orange_dam_audio_container.yml`.
  - In addition, the queue name configured in the migration file must also correspond to the `id`. So, following our example, the `queue_name` property for an 'Audio Container' `DocSubType` would be `queueName: 'orange_dam_migration:orange_dam_audio_container'`.
- Each migration _must_ be tagged with `orange_dam` within the `tags` configuration in order to register the associated queue where the data will be placed.
- Content migrations should be tagged with `orange_dam_content` and taxonomy based migrations should be tagged with `orange_dam_taxonomy`. This will not affect their functionality, but it will aid in filtering reporting data in Drush command output.

For a `CoreField.DocSubType` value of `Example Thing` the following would be required:

`migrate_plus.migration.orange_dam_example_thing.yml`:

```yaml
uuid: <UUID>
...
id: orange_dam_example_thing
...
migration_tags:
  - orange_dam
migration_group: orange_dam
...
source:
  plugin: queue
  queue_name: 'orange_dam_migration:orange_dam_example_thing'
...
```

#### Migration Tools

Various `@MigrateProcessPlugin` [plugins](./src/Plugin/migrate/process) are provided by this module to aid in the development of migrations.

#### Duplicate Item Detection
Orange Logic allows items to change content type. This can cause severe issues for downstream content managers, such as Drupal. For various reasons, we have opted to support this feature of Orange Logic, by, during migrations, checking if existing items were previously migrated in as an other content type. If that is the case, the earlier item is deleted, and all references to it are updated to point to the newly incoming item.

Generally, for incremental, on-going migrations, unless you know for a fact that no Orange Logic content items will never change type, it is recommended to configure orange_dam as follows:

```yml
check_for_duplicates: true
```

as that will cause the migration to check for whether the content type of the incoming item has changed and therefore needs its earlier incarnation to be deleted and re-created as its new content type (Orange Logic allows the same item to change content types).

However, if you know that your Orange Logic source content will never change type, you can set `check_for_duplicates` to `false`.

You can also temporarily override the duplicate checking. You may find this helpful during initial, bulk imports of data. To override duplicate checking during a large import or initial import, either temporarily reconfigure the above setting, or pass the `--disable-duplicate-check` option to the `orange-dam:migration-run*` commands.

Example: `drush orange-dam:migration-run-all --disable-duplicate-check`

This will significantly speed up your migrations.


### Cron Integration

#### Deletion Cron

This modules provides a [queue worker](./src/Plugin/QueueWorker/OrangeDamDeletionQueueWorker.php) for handling items in the deletion queue.

If you wish to override this functionality, the [`ultimate_cron`](https://www.drupal.org/project/ultimate_cron) module can be used to take over queue processing.

#### Syncing & Migration Cron

This module does not provide a default cron integration for requesting changed content or running migrations, as the timing of these calls is highly variable based on the specifics of the implementation.

If you are using [`ultimate_cron`](https://www.drupal.org/project/ultimate_cron), the following configuration is offered as a demonstration of automating content change requests every 5 minutes:

```yaml
-langcode: en
-status: true
-dependencies:
-  module:
-    - orange_dam
-title: 'Orange DAM - Sync'
-id: orange_dam_sync
-weight: 0
-module: orange_dam
-callback: 'orange_dam.queue_data_manager:syncUpdates'
-scheduler:
-  id: simple
-  configuration:
-    rules:
-      - '*/5+@ * * * *'
-launcher:
-  id: serial
-  configuration:
-    timeouts:
-      lock_timeout: 3600
-    launcher:
-      thread: 0
-logger:
-  id: database
-  configuration:
-    method: '3'
-    expire: 1209600
-    retain: 1000
```
