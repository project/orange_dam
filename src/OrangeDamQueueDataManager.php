<?php

namespace Drupal\orange_dam;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\State\State;
use Drupal\orange_dam\Event\OrangeDamDeletionQueueEvents;
use Drupal\orange_dam\Event\OrangeDamDeletionQueuePreDeleteEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manages retrieving a list of updated content and building a queue.
 */
class OrangeDamQueueDataManager implements OrangeDamQueueDataManagerInterface {

  /**
   * The log channel to use.
   *
   * @deprecated in orange_dam:2.2.0 and is removed from orange_dam:3.0.0. This
   *   constant has been superseded by logger.channel.orange_dam.queue service.
   * @see https://www.drupal.org/project/orange_dam/issues/3481057
   */
  public const LOGGER_CHANNEL = 'orange_dam.queue';

  /**
   * The format in which to capture, retrieve, and render queue state date-time.
   */
  public const STATE_DATETIME_FORMAT = 'c';

  /**
   * The state key that tracks the last time we checked for updates to content.
   */
  public const UPDATED_ITEM_STATE_TIME_KEY = 'orange_dam_item_update_last_run_time';

  /**
   * The state key that tracks the last time we checked for updates to keywords.
   */
  public const UPDATED_KEYWORD_STATE_TIME_KEY = 'orange_dam_keyword_update_last_run_time';

  /**
   * The state key that tracks the last time we checked for deleted content.
   */
  public const DELETED_CONTENT_STATE_TIME_KEY = 'orange_dam_deleted_content_last_run_time';

  /**
   * The state key that tracks the last time we checked for deleted keywords.
   */
  public const DELETED_KEYWORD_STATE_TIME_KEY = 'orange_dam_deleted_keyword_last_run_time';

  /**
   * The timezone object.
   */
  protected \DateTimeZone $timezone;

  /**
   * Constructor.
   */
  public function __construct(
    protected LoggerChannelInterface $logger,
    protected QueueFactory $queueFactory,
    protected OrangeDamApi $orangeDamApi,
    protected State $state,
    protected OrangeDamMigrationDataManager $migrationDataManager,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected OrangeDamConfigurationManager $orangeDamConfiguration,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
    $this->timezone = new \DateTimeZone('UTC');
  }

  /**
   * Queue all updated keywords and content.
   */
  public function syncUpdates(): void {
    $this->queueKeywordDeletions();
    $this->queueContentDeletions();
    $this->queueUpdatedKeywords();
    $this->queueUpdatedItems();
  }

  /**
   * Returns the logger for queue related issues.
   */
  public function logger(): LoggerChannelInterface {
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public function queueUpdatedItems(?DrupalDateTime $timeUpdatedSince = NULL): int|false {
    // Use the passed time if available. Fallback to the updated time state
    // value, which defaults to now if this is the first run.
    if (is_null($timeUpdatedSince)) {
      $timeUpdatedSince = new DrupalDateTime($this->state->get(static::UPDATED_ITEM_STATE_TIME_KEY, 'now'));
    }
    $timeUpdatedSince->setTimezone($this->timezone);
    $timeRequestStart = new DrupalDateTime('now', $this->timezone);
    // Queue the items updated since the given time until now.
    $itemCount = $this->orangeDamApi->queueContent($timeUpdatedSince, $timeRequestStart);
    // If we reach this point without an exception being thrown, update the
    // updated-since time to the current time and log the results.
    $this->orangeDamConfiguration->applyApiTimeOffset($timeRequestStart);
    $this->state->set(static::UPDATED_ITEM_STATE_TIME_KEY, $timeRequestStart->format(static::STATE_DATETIME_FORMAT));
    if ($itemCount) {
      $this->logger->notice(
        'Queued :count Orange DAM content item(s) with updates.',
        [
          ':count' => $itemCount,
        ]
      );
    }

    return $itemCount;
  }

  /**
   * {@inheritdoc}
   */
  public function queueUpdatedKeywords(?DrupalDateTime $timeUpdatedSince = NULL): int {
    // Use the passed time if available. Fallback to the updated time state
    // value, which defaults to now if this is the first run.
    if (is_null($timeUpdatedSince)) {
      $timeUpdatedSince = new DrupalDateTime($this->state->get(static::UPDATED_KEYWORD_STATE_TIME_KEY, 'now'));
    }
    $timeUpdatedSince->setTimezone($this->timezone);
    $timeNow = new DrupalDateTime('now', $this->timezone);
    $queued = 0;
    // Get the items updated since the given time until now.
    $keywords = $this->orangeDamApi->getKeywords($timeUpdatedSince, $timeNow);
    if ($keywords) {
      // Exceptions will be thrown on any API failure so we can assume success
      // from here, even if the results are empty.
      foreach ($keywords as $keyword) {
        // Queue each keyword item.
        $this->migrationDataManager->queueMigrationItem($keyword);
      }
      $queued = count($keywords);
      // Reduce log noise by only logging non-empty retrievals.
      $this->logger->notice(
        'Queued :count Orange DAM keywords for update.',
        [':count' => $queued]
      );
    }
    // Record the last time this was run for future checks.
    $this->state->set(static::UPDATED_KEYWORD_STATE_TIME_KEY, $timeNow->format(static::STATE_DATETIME_FORMAT));

    return $queued;
  }

  /**
   * Queue updates for all items of a given keyword type.
   *
   * @param string[] $keywordTypes
   *   The keyword types to queue updates for.
   *
   * @return int
   *   The number of queued keywords.
   */
  public function queueKeywordByType(array $keywordTypes): int {
    $queued = 0;
    // Validate the keyword types.
    foreach ($keywordTypes as $keywordType) {
      if (!$this->validateKeywordType($keywordType)) {
        $this->logger->error('Invalid keyword type :type requested for syncing.', [
          ':type' => $keywordType,
        ]);
      }
    }
    // Request the data from the API and queue the results.
    $keywords = $this->orangeDamApi->getKeywords(
      NULL,
      NULL,
      $keywordTypes,
    );
    if ($keywords) {
      // Exceptions will be thrown on any API failure so we can assume success
      // from here, even if the results are empty.
      foreach ($keywords as $keyword) {
        $this->migrationDataManager->queueMigrationItem($keyword);
      }
      $queued = count($keywords);
    }

    // Log the results.
    $this->logger->notice(
      'Queueing :count Orange DAM :type keywords(s) for re-syncing.', [
        ':count' => $queued,
        ':type' => implode(', ', $keywordTypes),
      ]);
    return $queued;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSingleItem(string $systemIdentifier): OrangeDamItemInterface|false {
    if ($item = $this->orangeDamApi->queueContentItem($systemIdentifier)) {
      $this->logger->notice(
        'Orange DAM item :system_identifier has been successfully queued for updating in the :queue queue.', [
          ':system_identifier' => $systemIdentifier,
          ':queue' => $this->migrationDataManager->getItemQueueName($item),
        ]);
      return $item;
    }
    $this->logger->error(
      'Failed to retrieve Orange DAM item :system_id from the Orange DAM API.', [
        ':system_id' => $systemIdentifier,
      ]
    );
    return FALSE;
  }

  /**
   * Queue updates for all items of a given content type.
   *
   * @param array $contentTypes
   *   The content types to queue updates for.
   * @param int $limit
   *   The maximum number of items, rounded to the nearest page size.
   * @param int $pageNumber
   *   The optional initial page number for paged requests.
   * @param string $sort
   *   The optional Orang Logic API sort option.
   */
  public function queueContentByType(array $contentTypes, int $limit = 0, int $pageNumber = OrangeDamApi::INITIAL_PAGE_NUMBER, string $sort = ''): void {
    $itemCount = 0;
    // Validate the content types.
    foreach ($contentTypes as $contentType) {
      if (!$this->validateContentType($contentType)) {
        $this->logger->error(
          'Invalid content type :type requested for syncing.', [
            ':type' => $contentType,
          ]);
        continue;
      }
      // Request the data from the API and queue the results.
      $itemCount += $this->orangeDamApi->queueContent(
        NULL,
        NULL,
        [$contentType],
        $limit,
        $pageNumber,
        $sort
      );
    }
    // Record the total number of items queued.
    $this->logger->notice(
      'Queued :count Orange DAM :types content item(s).',
      [
        ':count' => $itemCount,
        ':types' => implode(', ', $contentTypes),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function queueContentDeletions(?DrupalDateTime $timeDeletedSince = NULL): int|false {
    // Use the passed time if available. Fallback to the deleted time state
    // value, which defaults to now if this is the first run.
    if (is_null($timeDeletedSince)) {
      $timeDeletedSince = new DrupalDateTime($this->state->get(static::DELETED_CONTENT_STATE_TIME_KEY, 'now'));
    }
    $timeDeletedSince->setTimezone($this->timezone);
    $timeNow = new DrupalDateTime('now', $this->timezone);
    // Get the items updated since the given time until now.
    $itemCount = $this->orangeDamApi->getDeletedContent($timeDeletedSince, $timeNow);
    // Don't set new timestamp if query failed.
    if ($itemCount === FALSE) {
      return FALSE;
    }
    elseif ($itemCount > 0) {
      $this->logger->notice(
        'Queued :count Orange DAM content items for deletion.',
        [':count' => $itemCount]
      );
    }
    // Record the time since the request succeeded.
    $this->state->set(static::DELETED_CONTENT_STATE_TIME_KEY, $timeNow->format(static::STATE_DATETIME_FORMAT));
    // Display a final summary.
    return $itemCount;
  }

  /**
   * {@inheritdoc}
   */
  public function queueKeywordDeletions(?DrupalDateTime $timeDeletedSince = NULL): int|false {
    // Use the passed time if available. Fallback to the deleted time state
    // value, which defaults to now if this is the first run.
    if (is_null($timeDeletedSince)) {
      $timeDeletedSince = new DrupalDateTime($this->state->get(static::DELETED_KEYWORD_STATE_TIME_KEY, 'now'));
    }
    $timeDeletedSince->setTimezone($this->timezone);
    $timeNow = new DrupalDateTime('now', $this->timezone);
    // Get the items updated since the given time until now.
    $itemCount = $this->orangeDamApi->getDeletedKeywords($timeDeletedSince);
    if ($itemCount === FALSE) {
      return FALSE;
    }
    elseif ($itemCount > 0) {
      $this->logger->notice(
        'Queued :count Orange DAM keywords for deletion.',
        [':count' => $itemCount]
      );
    }
    // Record the time since the request succeeded.
    $this->state->set(static::DELETED_KEYWORD_STATE_TIME_KEY, $timeNow->format(static::STATE_DATETIME_FORMAT));
    return $itemCount;
  }

  /**
   * Deletes an Orange DAM item during queue processing.
   *
   * @param \Drupal\orange_dam\OrangeDamItemInterface $item
   *   The Orange DAM item to use as the source of the deletion operation.
   *
   * @return int
   *   The number of items that were processed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see \Drupal\orange_dam\Plugin\QueueWorker\OrangeDamDeletionQueueWorker
   */
  public function processDeletedQueueItem(OrangeDamItemInterface $item): int {
    $processed = 0;
    /** @var \Drupal\orange_dam\Event\OrangeDamDeletionQueuePreDeleteEvent $preDeleteEvent */
    $preDeleteEvent = $this->eventDispatcher->dispatch(
      new OrangeDamDeletionQueuePreDeleteEvent($item),
      OrangeDamDeletionQueueEvents::PRE_DELETE
    );
    if ($preDeleteEvent->itemHandled()) {
      return ++$processed;
    }

    if (!$migration = $this->migrationDataManager->getMigrationBySourceId($item->id())) {
      $this->logger->warning('No migrated content found having source id ":id" while processing the deletion queue.', [
        ':id' => $item->id(),
      ]);
      return $processed;
    }
    // Get the destination entity ID and type values.
    $entityIds = $migration->getIdMap()->lookupDestinationIds([$item->id()]);
    // The result of the destination ID lookup above returns a nested array of
    // source keys and their 1-to-many destination keys even when there are no
    // results, so we filter the results to simplify the empty check below.
    $entityIds = array_filter(array_pop($entityIds));
    // @todo Address upstream the fact that getDerivativeId() is not
    //   guaranteed to exist on destination plugins.
    // @see https://www.drupal.org/project/drupal/issues/2783715
    $entityType = $migration->getDestinationPlugin()->getDerivativeId();
    // Validate that a destination entity was found.
    if ($entityIds === []) {
      $this->logger->error('No migration destination id found for item ":type" ":id" while processing the deletion queue.', [
        ':id' => $item->id(),
        ':type' => $item->type(),
      ]);
      return $processed;
    }
    // Load the entities.
    $entities = $this->entityTypeManager
      ->getStorage($entityType)
      ->loadMultiple($entityIds);
    // Validate that entities were found.
    if ($entities === []) {
      $this->logger->error('Migration destination entity :id (from ":type" ":source_id") could not be found while processing the deletion queue.', [
        ':id' => implode(', ', $entityIds),
        ':type' => $item->type(),
        ':source_id' => $item->id(),
      ]);
      return $processed;
    }
    // Delete the entities.
    foreach ($entities as $entity) {
      try {
        $entity->delete();
        $this->logger->notice('Deleted entity (:type::id) ":title" during Orange DAM deletion syncing.', [
          ':type' => $entity->getEntityType()->id(),
          ':id' => $entity->id(),
          ':title' => $entity->label(),
        ]);
        $processed++;
      }
      catch (EntityStorageException $e) {
        throw new SuspendQueueException(
          'Failed to delete entity (' . $entity->getEntityTypeId() . ':' . $entity->id() . ') "' . $entity->label() . '" during Orange DAM deletion syncing.',
          $e->getCode(),
          $e
        );
      }
    }
    return $processed;
  }

  /**
   * Validate if a content type is valid.
   *
   * @param string $contentType
   *   The content type name.
   *
   * @return bool
   *   Indication if the content type is valid.
   */
  public function validateContentType(string $contentType): bool {
    return in_array($contentType, $this->orangeDamConfiguration->getContentTypeNames());
  }

  /**
   * Validate if a keyword type is valid.
   *
   * @param string $keywordType
   *   The keyword type name.
   *
   * @return bool
   *   Indication if the keyword type is valid.
   */
  public function validateKeywordType(string $keywordType): bool {
    return in_array($keywordType, $this->orangeDamConfiguration->getKeywordTypes());
  }

  /**
   * Generate the URL to a file asset for a given item.
   *
   * The content type is optional, but if it is omitted it adds a second API
   * call to this process.
   *
   * @param string $systemId
   *   The item's System ID.
   * @param string|null $contentType
   *   The item's content type.
   *
   * @return string|false
   *   The asset URL if one was generated or false if nothing was generated.
   */
  public function getAssetUrl(string $systemId, ?string $contentType = NULL): string|false {
    // Get the content type of item if it was not passed in.
    if (is_null($contentType)) {
      $item = $this->orangeDamApi->getContentItem($systemId);
      $contentType = $item ? $item->type() : NULL;
    }
    // Confirm that a content type was found.
    if (!$contentType) {
      $this->logger->error(
        'Unable to determine the content type of the item with system ID :system_id for asset URL generation.', [
          ':system_id' => $systemId,
        ]);
      return FALSE;
    }
    // Get the asset format and verify that a value was found.
    $assetFormat = $this->orangeDamConfiguration->getAssetFormat($contentType);
    if (!$assetFormat) {
      return FALSE;
    }
    // Generate the asset URL.
    return $this->orangeDamApi->getAssetUrl(
      $systemId,
      $assetFormat,
    );
  }

}
