<?php

namespace Drupal\orange_dam;

/**
 * The representation of an Orange DAM object.
 */
class OrangeDamContent implements OrangeDamItemInterface {

  /**
   * Some API responses don't include the item type, so set a default.
   */
  public const DEFAULT_CONTENT_TYPE = '-unset-';

  /**
   * The item type.
   */
  public const ITEM_TYPE = 'orange_dam_content';

  /**
   * The content type property.
   */
  public const CONTENT_TYPE_PROPERTY = 'CoreField.DocSubType';

  /**
   * The systemIdentifier value.
   */
  public string $systemIdentifier;

  /**
   * The JSON object data.
   */
  public object $data;

  /**
   * The item type.
   */
  public string $type;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $id, object $data, string $type = self::DEFAULT_CONTENT_TYPE) {
    static::validateSystemIdentifier($id);
    $this->systemIdentifier = $id;
    $this->data = $data;
    $this->type = $type;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->systemIdentifier;
  }

  /**
   * {@inheritdoc}
   */
  public function data(): object {
    $data = $this->data;
    $data->{static::ITEM_TYPE_PROPERTY} = static::ITEM_TYPE;
    $data->{static::CONTENT_TYPE_PROPERTY} = $this->type();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function type(): string {
    return $this->type;
  }

  /**
   * Validates a system identifier.
   *
   * @param string $systemIdentifier
   *   The Orange DAM system identifier.
   *
   * @return bool
   *   Indication if the system identifier matches the valid pattern.
   *   e.g. EX1STO431061
   *
   * @throws \Exception
   *   If system identifier is invalid.
   */
  public static function validateSystemIdentifier(string $systemIdentifier): bool {
    $regex = \Drupal::config('orange_dam.settings')->get('system_identifier_pattern');
    if (empty($regex)) {
      throw new \Exception('Missing Orange DAM system identifier validation pattern configuration.');
    }
    $valid = preg_match($regex, $systemIdentifier);
    if (!$valid) {
      throw new \Exception(sprintf('Invalid Orange DAM system identifier "%s".', $systemIdentifier));
    }
    return TRUE;
  }

}
