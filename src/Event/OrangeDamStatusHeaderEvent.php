<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Allows Orange DAM status headers to be altered.
 */
class OrangeDamStatusHeaderEvent extends Event {

  /**
   * Creates the event.
   */
  public function __construct(
    protected array $headers,
  ) {}

  /**
   * Get the table headers.
   */
  public function getHeaders(): array {
    return $this->headers;
  }

  /**
   * Set the table headers.
   */
  public function setHeaders(array $headers): void {
    $this->headers = $headers;
  }

}
