<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\orange_dam\OrangeDamItemInterface;

/**
 * Allow custom handling of deletion queue items before deletion.
 */
class OrangeDamDeletionQueuePreDeleteEvent extends Event {

  /**
   * Should the item be considered handled after this event?
   */
  protected bool $handled = FALSE;

  /**
   * Creates an Orange DAM deletion queue item pre-delete event.
   */
  public function __construct(
    protected OrangeDamItemInterface $item,
  ) {}

  /**
   * Get the item that is in the deletion queue and about to be deleted.
   */
  public function getItem(): OrangeDamItemInterface {
    return $this->item;
  }

  /**
   * Return whether the item was handled.
   */
  public function itemHandled(): bool {
    return $this->handled;
  }

  /**
   * Set whether the item should be considered handled or not.
   */
  public function setHandled(bool $handled) {
    $this->handled = $handled;
  }

}
