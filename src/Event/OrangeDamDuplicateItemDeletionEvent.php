<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Allow for customized duplicate entity deletion.
 */
class OrangeDamDuplicateItemDeletionEvent extends Event {

  /**
   * Creates an Orange DAM duplicate item deletion event.
   *
   * @param \Drupal\Core\Entity\EntityInterface $deletedEntity
   *   The duplicate entity we are about to delete.
   * @param \Drupal\Core\Entity\EntityInterface $newEntity
   *   The new migrated entity that is replacing the old duplicate one.
   */
  public function __construct(
    protected EntityInterface $deletedEntity,
    protected EntityInterface $newEntity,
  ) {}

  /**
   * Get the duplicate entity we are about to delete.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The duplicate entity we are about to delete.
   */
  public function getDeletedEntity(): EntityInterface {
    return $this->deletedEntity;
  }

  /**
   * Get the new migrated entity that is replacing the old duplicate one.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The new migrated entity that is replacing the old duplicate one.
   */
  public function getNewEntity(): EntityInterface {
    return $this->newEntity;
  }

}
