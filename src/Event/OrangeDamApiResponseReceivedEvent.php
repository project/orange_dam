<?php

namespace Drupal\orange_dam\Event;

use Chromatic\OrangeDam\Http\Response;
use Drupal\Component\EventDispatcher\Event;

/**
 * Allows Orange DAM API parameters to be altered.
 */
class OrangeDamApiResponseReceivedEvent extends Event {

  /**
   * An identifier for the request type that produced the response.
   *
   * This should help a listener/subscriber key in on the response received
   *   events they truly are interested in. We assume the path of the endpoint
   *   should be a pretty good id, so we do so in the constructor.
   */
  public readonly string $id;

  /**
   * Creates a Orange DAM API Response event.
   */
  public function __construct(protected Response $response) {
    $this->id = $response->getRequestPath();
  }

  /**
   * Gets the response.
   */
  public function getResponse(): Response {
    return $this->response;
  }

}
