<?php

namespace Drupal\orange_dam\Event;

/**
 * Defines events for Orange DAM status functionality.
 */
final class OrangeDamStatusEvents {

  /**
   * The event fired when querying for status reporting.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamStatusQueryEvent
   *
   * @var string
   */
  public const ALTER_QUERY = 'orange_dam.alter_query';

  /**
   * The event fired when building a status result row.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamStatusRowEvent
   *
   * @var string
   */
  public const ALTER_ROW = 'orange_dam.alter_row';

  /**
   * The event fired when building status table headers.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamStatusHeaderEvent
   *
   * @var string
   */
  public const ALTER_HEADER = 'orange_dam.alter_header';

}
