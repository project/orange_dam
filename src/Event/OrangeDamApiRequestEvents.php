<?php

namespace Drupal\orange_dam\Event;

/**
 * Defines events for Orange DAM API requests.
 */
final class OrangeDamApiRequestEvents {

  /**
   * The event fired when retrieving a single item's data.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamApiRequestEvent
   *
   * @var string
   */
  public const ITEM_DATA = 'orange_dam.item_data';

  /**
   * The event fired when retrieving all updated items.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamApiRequestEvent
   *
   * @var string
   */
  public const UPDATED_ITEMS = 'orange_dam.updated_items';

}
