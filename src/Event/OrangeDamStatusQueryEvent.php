<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * Allows Orange DAM status filters to be altered.
 */
class OrangeDamStatusQueryEvent extends Event {

  /**
   * Creates the event.
   */
  public function __construct(
    protected array $options,
    protected SelectInterface $query,
  ) {}

  /**
   * Get the query filter options.
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * Get the database query.
   */
  public function getQuery(): SelectInterface {
    return $this->query;
  }

}
