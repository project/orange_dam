<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\orange_dam\OrangeDamContent;

/**
 * Allow for customized duplicate entity detection.
 */
class OrangeDamDuplicateItemDetectionEvent extends Event {

  /**
   * The duplicate entities.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $entities = [];

  /**
   * Creates an Orange DAM duplicate item event.
   *
   * @param \Drupal\orange_dam\OrangeDamContent $item
   *   The item to check for duplicates.
   * @param \Drupal\Core\Entity\EntityInterface $migratedEntity
   *   The migrated entity to check for duplicates against.
   */
  public function __construct(
    protected OrangeDamContent $item,
    protected EntityInterface $migratedEntity,
  ) {
  }

  /**
   * Get the source item to check for duplicates against.
   *
   * @return \Drupal\orange_dam\OrangeDamContent
   *   The source item to check for duplicates against.
   */
  public function getItem(): OrangeDamContent {
    return $this->item;
  }

  /**
   * Get the migrated entity to check for duplicates against.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The migrated entity to check for duplicates against.
   */
  public function getMigratedEntity(): EntityInterface {
    return $this->migratedEntity;
  }

  /**
   * Add an entity to the duplicate entity list.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The duplicate entity.
   */
  public function addDuplicateEntity(EntityInterface $entity): void {
    $this->entities[] = $entity;
  }

  /**
   * The duplicate entities if some were found.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The duplicate entities if some were found.
   */
  public function getDuplicateEntities(): array {
    return $this->entities;
  }

}
