<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Allows Orange DAM API parameters to be altered.
 */
class OrangeDamApiRequestEvent extends Event {

  /**
   * The array of Orange DAM parameters.
   *
   * @var array
   */
  protected $parameters;

  /**
   * The current message service.
   *
   * @var \Drupal\migrate\MigrateMessageInterface
   */
  protected $message;

  /**
   * Creates a Orange DAM API event.
   *
   * @param array $parameters
   *   The API parameters to alter.
   */
  public function __construct(array $parameters) {
    $this->parameters = $parameters;
  }

  /**
   * Gets the parameters.
   *
   * @return array
   *   The parameters
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Sets API parameters.
   *
   * @param array $parameters
   *   The API parameters.
   */
  public function setParameters(array $parameters) {
    $this->parameters = $parameters;
  }

}
