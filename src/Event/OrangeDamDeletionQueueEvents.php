<?php

namespace Drupal\orange_dam\Event;

/**
 * Defines events for Orange DAM duplicate item detection.
 */
final class OrangeDamDeletionQueueEvents {

  /**
   * The event fired before processing a deletion queue item.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamDeletionQueuePreDeleteEvent
   *
   * @var string
   */
  public const PRE_DELETE = 'orange_dam.deletion_queue.pre_delete';

}
