<?php

namespace Drupal\orange_dam\Event;

/**
 * Defines events for Orange DAM API responses.
 */
final class OrangeDamApiResponseEvents {
  /**
   * The event fired when a response to an API query is received.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamApiResponseReceivedEvent
   */
  const RECEIVED = OrangeDamApiResponseReceivedEvent::class;

}
