<?php

namespace Drupal\orange_dam\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Allows Orange DAM status rows to be altered.
 */
class OrangeDamStatusRowEvent extends Event {

  /**
   * Creates the event.
   */
  public function __construct(
    protected array $row,
    protected object $result,
    protected ?FieldableEntityInterface $entity,
  ) {}

  /**
   * Get the table row.
   */
  public function getRow(): array {
    return $this->row;
  }

  /**
   * Get the query row result.
   */
  public function getResult(): object {
    return $this->result;
  }

  /**
   * Get the entity associated with the row.
   */
  public function getEntity(): ?FieldableEntityInterface {
    return $this->entity;
  }

  /**
   * Set the row.
   */
  public function updateRow(array $row): void {
    $this->row = $row;
  }

}
