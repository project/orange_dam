<?php

namespace Drupal\orange_dam\Event;

/**
 * Defines events for Orange DAM duplicate item detection.
 */
final class OrangeDamDuplicateItemEvents {

  /**
   * The event fired when finding duplicate items.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamDuplicateItemDetectionEvent
   *
   * @var string
   */
  public const DETECTED = 'orange_dam.duplicate_item.detected';

  /**
   * The event fired when deleting a duplicate item.
   *
   * @Event
   *
   * @see \Drupal\orange_dam\Event\OrangeDamDuplicateItemDetectionEvent
   *
   * @var string
   */
  public const DELETED = 'orange_dam.duplicate_item.deleted';

}
