<?php

namespace Drupal\orange_dam\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\orange_dam\Event\OrangeDamStatusEvents;
use Drupal\orange_dam\Event\OrangeDamStatusHeaderEvent;
use Drupal\orange_dam\Event\OrangeDamStatusQueryEvent;
use Drupal\orange_dam\Event\OrangeDamStatusRowEvent;
use Drupal\orange_dam\OrangeDamApi;
use Drupal\orange_dam\OrangeDamConfigurationManager;
use Drupal\orange_dam\OrangeDamContent;
use Drupal\orange_dam\OrangeDamItemInterface;
use Drupal\orange_dam\OrangeDamMigrationDataManager;
use Drupal\orange_dam\OrangeDamQueueDataManager;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\CommandFailedException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Builds Orange DAM related drush commands.
 */
class OrangeDamCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The maximum length of a title in the command output.
   */
  public const MAX_TITLE_CHARACTER_LENGTH = 60;

  /**
   * The maximum length of a field name in the command output.
   */
  public const MAX_FIELD_CHARACTER_LENGTH = 80;

  /**
   * The limit to use when migrating a single item.
   */
  public const SINGLE_ITEM_MIGRATION_LIMIT = 1;

  /**
   * Builds the drush commands.
   */
  public function __construct(
    protected OrangeDamQueueDataManager $orangeDamUpdatedDataManager,
    protected OrangeDamMigrationDataManager $orangeDamMigrationDataManager,
    protected OrangeDamApi $orangeDamApi,
    protected OrangeDamConfigurationManager $orangeDamConfigurationManager,
    protected QueueFactory $queueFactory,
    protected QueueWorkerManager $queueManager,
    protected Connection $databaseConnection,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EventDispatcherInterface $eventDispatcher,
    protected LoggerChannelInterface $migrationLogger,
    protected LoggerChannelInterface $queueLogger,
  ) {
    parent::__construct();
  }

  /**
   * Sync deletions and queue all updated items since the last sync.
   *
   * @command orange-dam:sync-updates
   * @aliases od-su
   *
   * @usage orange-dam:su
   *   Sync deletions and queue all updated items since the last sync.
   */
  public function syncUpdates(): void {
    $this->orangeDamUpdatedDataManager->syncUpdates();
    $this->displayMigrationQueueListing();
  }

  /**
   * Queue a single content item to be updated from Orange DAM.
   *
   * @param string $systemIdentifier
   *   The system identifier of the item in Orange DAM. Example: EX1STO431061.
   * @param array $options
   *   Command options.
   *
   * @command orange-dam:queue-content-item
   * @aliases od-qci
   *
   * @option disable-duplicate-check
   *   Disable duplicate item detection.
   *
   * @usage orange-dam:queue-content-item <systemIdentifier>
   *   Queue an update for a single item from Orange DAM.
   * @usage orange-dam:queue-content-item <systemIdentifier> --disable-duplicate-check
   *   Queue an update for a single item without duplicate checks.
   */
  public function queueContentItem(
    string $systemIdentifier,
    // @todo This option seems only partially implemented and is unused.
    array $options = [
      'disable-duplicate-check' => FALSE,
    ],
  ): OrangeDamItemInterface|int {
    try {
      OrangeDamContent::validateSystemIdentifier($systemIdentifier);
    }
    catch (\Exception $e) {
      $this->io()->error($e->getMessage());
      return self::EXIT_FAILURE;
    }
    // Retrieve the item.
    $item = $this->orangeDamUpdatedDataManager->updateSingleItem($systemIdentifier);
    if ($item) {
      $this->io()->info(dt(':type :id has been queued', [':type' => $item->type(), ':id' => $item->id()]));
      return $item;
    }
    else {
      $this->io()->error(dt("Failed to queue :id.", [':id' => $systemIdentifier]));
      return self::EXIT_FAILURE;
    }
  }

  /**
   * Queue and migrate a single content item from Orange DAM.
   *
   * @param string $systemIdentifier
   *   The system identifier of the item in Orange DAM. Example: EX1STO431061.
   * @param array $options
   *   Command options.
   *
   * @command orange-dam:queue-migrate-content-item
   * @aliases od-qmci
   *
   * @option disable-duplicate-check
   *   Disable duplicate item detection.
   *
   * @usage orange-dam:queue-migrate-content-item <systemIdentifier>
   *   Queue an update for a single item from Orange DAM.
   * @usage orange-dam:queue-migrate-content-item <systemIdentifier> --disable-duplicate-check
   *   Queue an update for a single item without duplicate checks.
   */
  public function queueAndMigrateContentItem(
    string $systemIdentifier,
    array $options = [
      'disable-duplicate-check' => FALSE,
    ],
  ): int {
    $item = $this->queueContentItem($systemIdentifier, $options);
    if ($item instanceof OrangeDamItemInterface) {
      // Run the migrations.
      $options['limit'] = static::SINGLE_ITEM_MIGRATION_LIMIT;
      $options['idlist'] = $systemIdentifier;
      $migration = $this->orangeDamMigrationDataManager->getItemMigration($item);
      $migrations = $migration ? [$migration->id()] : [];
      $results = $this->orangeDamMigrationDataManager->runMigrations(
        $migrations,
        $options,
        (bool) $options['disable-duplicate-check'],
      );
      $this->displayMigrationResults($results);
      return self::EXIT_SUCCESS;
    }
    else {
      return self::EXIT_FAILURE;
    }
  }

  /**
   * Queue all content updated since a given time.
   *
   * @param string $time
   *   Full date time string in UTC of any PHP parsable format, such as
   *   "1/25/2023 09:47:01".
   *
   * @command orange-dam:queue-content-updated-since-time
   * @aliases od-qcust
   *
   * @usage orange-dam:queue-content-updated-since-time <php parseable date time string>
   *   Queue all content updated since a given time.
   */
  public function queueContentUpdatedSinceTime(string $time): void {
    $time = $this->createDateTime($time);
    $this->orangeDamUpdatedDataManager->queueUpdatedItems($time);
    $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_CONTENT]);
  }

  /**
   * Queue all content updated since the last sync.
   *
   * @command orange-dam:queue-content-updated-since-last-sync
   * @aliases od-qcusls
   *
   * @usage orange-dam:queue-content-updated-since-last-sync
   *   Queue all content updated since the last sync.
   */
  public function queueContentUpdatedSinceLastSync() {
    $this->orangeDamUpdatedDataManager->queueUpdatedItems();
    $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_CONTENT]);
  }

  /**
   * Queue all content of a given type for syncing.
   *
   * @param array $options
   *   The command options.
   *
   * @command orange-dam:queue-content-by-type
   * @aliases od-qcbt
   *
   * @option type
   *   Allows for the item type to be specified.
   * @option page
   *   Optional page number at which to start paging the request.
   * @option sort
   *   Optional Orange Logic sort type.
   *
   * @usage orange-dam:queue-content-by-type
   *   Queue all content for a given content type.
   * @usage orange-dam:queue-content-by-type --page=3
   *    Queue all content for a given content type starting with page 3.
   * @usage orange-dam:queue-content-by-type --type='Example Type'
   *    Queue all content of the "Example Type" content type.
   * @usage orange-dam:queue-content-by-type --type='Example Type' --sort='your sort option'
   *    Queue all content of the "Example Type" content type sorted by the
   *    specified Orange Logic sort option.
   */
  public function queueContentByType(
    array $options = [
      'type' => '',
      'page' => '',
      'sort' => '',
    ],
  ) {
    $contentType = $options['type'];
    if ($contentType) {
      if (!$this->orangeDamUpdatedDataManager->validateContentType($contentType)) {
        $this->io()->error(dt(
          ':type is not a valid content type.', [
            ':type' => $contentType,
          ]));
        return;
      }
    }
    else {
      $contentTypes = $this->orangeDamConfigurationManager->getContentTypeNames();
      $choice = $this->io()->choice('Content type to sync from Orange DAM:', $contentTypes);
      $contentType = $contentTypes[$choice];
    }
    $args = ['contentTypes' => [$contentType]];
    // Leverage the page number if one was provided.
    if (is_numeric($options['page']) && $options['page'] > 1) {
      $args['pageNumber'] = (int) $options['page'];
    }
    if (strlen($options['sort']) > 0) {
      $args['sort'] = $options['sort'];
    }
    call_user_func_array(
      [$this->orangeDamUpdatedDataManager, 'queueContentByType'],
      $args
    );
    $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_CONTENT]);
  }

  /**
   * Queue all content for syncing.
   *
   * @command orange-dam:queue-content-all
   * @aliases od-qca
   *
   * @option limit
   *   Allows for a limit to be specified for each item type.
   *
   * @usage orange-dam:queue-content-all
   *   Queue all content for syncing.
   * @usage orange-dam:queue-content-all --limit=99
   *   Queue all content types for syncing, but only 99 items of each.
   */
  public function queueContentAll(
    array $options = [
      'limit' => '',
    ],
  ) {
    $limit = $options['limit'] ?: 0;
    $this->orangeDamUpdatedDataManager->queueContentByType(
      $this->orangeDamConfigurationManager->getContentTypeNames(),
      (int) $limit,
    );
    $this->displayMigrationQueueListing();
  }

  /**
   * Queue updated Orange DAM keywords since the last sync.
   *
   * @command orange-dam:queue-keywords-updated-since-last-sync
   * @aliases od-quksls
   *
   * @usage orange-dam:queue-keywords-updated-since-last-sync
   *   Queue all updated Orange DAM keywords since the last sync.
   */
  public function queueKeywordsUpdatedSinceLastSync() {
    $queued = $this->orangeDamUpdatedDataManager->queueUpdatedKeywords();
    if ($queued) {
      $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_TAXONOMY]);
      $this->io()->writeln(dt(':count keyword(s) queued for update.', [':count' => $queued]));
    }
    else {
      $this->io()->writeln(dt('No keywords were queued for update.'));
    }
  }

  /**
   * Queue updated Orange DAM keywords since a given time.
   *
   * @param string $time
   *   Full date time string in UTC of any PHP parsable format, such as
   *   "1/25/2023 09:47:01".
   *
   * @command orange-dam:queue-keywords-updated-since-time
   * @aliases od-qukst
   *
   * @usage orange-dam:queue-keywords-updated-since-time
   *   Queue all updated Orange DAM keywords since a given time.
   */
  public function queueKeywordsUpdatedSinceTime(string $time) {
    $time = $this->createDateTime($time);
    $queued = $this->orangeDamUpdatedDataManager->queueUpdatedKeywords($time);
    if ($queued) {
      $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_TAXONOMY]);
      $this->io()->writeln(dt(':count keyword(s) queued for update.', [':count' => $queued]));
    }
    else {
      $this->io()->writeln(dt('No keywords were queued for update.'));
    }
  }

  /**
   * Queue Orange DAM keywords by type.
   *
   * @command orange-dam:queue-keywords-by-type
   * @aliases od-qkbt
   *
   * @usage orange-dam:queue-keywords-by-type --type="Example Type"
   *   Queue all updated Orange DAM keywords of a given type.
   * @usage orange-dam:queue-keywords-by-type
   *   Queue all updated Orange DAM keywords of a given type from the prompt.
   */
  public function queueKeywordsByType(
    $options = [
      'type' => '',
    ],
  ) {
    $tagType = $options['type'];
    if ($tagType) {
      if (!$this->orangeDamUpdatedDataManager->validateKeywordType($tagType)) {
        $this->io()->error(dt(
          'Keyword type :type is not a valid keyword type.', [
            ':type' => $tagType,
          ]));
        return;
      }
    }
    else {
      $tagTypes = $this->orangeDamConfigurationManager->getKeywordTypes();
      $choice = $this->io()->choice('Keyword type to sync from Orange DAM:', $tagTypes);
      $tagType = $tagTypes[$choice];
    }
    $this->orangeDamUpdatedDataManager->queueKeywordByType([$tagType]);
    $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_TAXONOMY]);
    $this->displayUnregisteredQueueCount();
  }

  /**
   * Queue all keywords for migrations.
   *
   * @command orange-dam:queue-keywords-all
   * @aliases od-qka
   *
   * @usage orange-dam:queue-keywords-all
   *   Queue all content for migrations.
   */
  public function queueKeywordsAll() {
    $this->orangeDamUpdatedDataManager->queueKeywordByType([]);
    $this->displayMigrationQueueListing([OrangeDamMigrationDataManager::MIGRATION_TAG_TAXONOMY]);
  }

  /**
   * Queue all content deletions since a given time.
   *
   * @param string $time
   *   Full date time string in UTC of any PHP parsable format, such as
   *   "1/25/2023 09:47:01".
   *
   * @command orange-dam:queue-deleted-content-since-time
   * @aliases od-qdcst
   *
   * @usage orange-dam:queue-deleted-content-since-time 1/23/2023
   *   Queue all content deletions since a given time.
   */
  public function queueDeletedContentSinceTime(string $time) {
    $time = $this->createDateTime($time);
    $this->orangeDamUpdatedDataManager->queueContentDeletions($time);
  }

  /**
   * Queue all content deletions since the last sync.
   *
   * @command orange-dam:queue-deleted-content-since-last-sync
   * @aliases od-qdcsls
   *
   * @usage orange-dam:queue-deleted-content-since-last-sync
   *   Queue all content deletions since the last sync.
   */
  public function queueDeletionsContentSinceLastSync() {
    $this->orangeDamUpdatedDataManager->queueContentDeletions();
  }

  /**
   * Queue keywords deleted since the given time.
   *
   * @command orange-dam:queue-deleted-keywords-since-time
   * @aliases od-qdkst
   *
   * @usage orange-dam:queue-deleted-keywords-since-time 1/23/2023
   *   Queue keyword deletions since the given time.
   */
  public function queueDeletedKeywordsSinceTime(string $time) {
    $time = $this->createDateTime($time);
    $this->orangeDamUpdatedDataManager->queueKeywordDeletions($time);
  }

  /**
   * Queue keywords deleted since the last sync.
   *
   * @command orange-dam:queue-deleted-keywords-since-last-sync
   * @aliases od-qdksls
   *
   * @usage orange-dam:queue-deleted-keywords-since-last-sync
   *   Queue all keyword deletions since the last sync.
   */
  public function queueDeletedKeywordsSinceLastSync() {
    $this->orangeDamUpdatedDataManager->queueKeywordDeletions();
  }

  /**
   * Run all Orange DAM migrations.
   *
   * @command orange-dam:migration-run-all
   * @aliases od-mra
   *
   * @option limit
   *   The maximum number of items to migrate.
   * @option disable-duplicate-check
   *   Disable duplicate item checking. Useful when importing large data sets
   *   at a time. This option is a flag and does not require a value.
   *   Example orange-dam:migration-run-all --disable-duplicate-check
   *
   * @usage orange-dam:migration-run-all
   *   Run the Orange DAM migrations with the default limit.
   * @usage orange-dam:migration-run-all --limit=123
   *   Run the Orange DAM migrations with a custom limit.
   */
  public function migrationRunAll(
    array $options = [
      'limit' => '',
      'disable-duplicate-check' => FALSE,
    ],
  ) {
    $options['limit'] = $options['limit'] ?: OrangeDamMigrationDataManager::MIGRATION_LIMIT;
    $this->migrationLogger->notice(dt(
      'Running all Orange DAM related migrations.'
    ));
    $results = $this->orangeDamMigrationDataManager->runMigrations(
      [],
      $options,
      (bool) $options['disable-duplicate-check'],
    );
    $this->displayMigrationResults($results);
    $this->displayMigrationQueueListing();
  }

  /**
   * Run a specific Orange DAM migration.
   *
   * @param array $options
   *   The command options.
   *
   * @command orange-dam:migration-run
   * @aliases od-mr
   *
   * @option migration
   *   Allows for the migration to be specified.
   * @option limit
   *   The maximum number of items to migrate.
   * @option idlist
   *   The comma separated source ID of the items to migrate.
   * @option group
   *   A comma-separated list of migration groups to run.
   * @option disable-duplicate-check
   *   Disable duplicate item checking. Useful when importing large data sets
   *   at a time. This option is a flag and does not require a value.
   *   Example orange-dam:migration-run-all --disable-duplicate-check
   *
   * @usage orange-dam:migration-run --limit=123
   *   Run a specified Orange DAM migration with the limit of 123.
   * @usage orange-dam:migration-run --migration=example_migration
   *   Run the specified Orange DAM migration with no limit.
   * @usage orange-dam:migration-run --idlist=1,2,3
   *   Run the specified Orange DAM migration for the specified items.
   * @usage orange-dam:migration-run --group=foo,bar
   *   Run the specified Orange DAM migration only on the passed group(s).
   */
  public function migrationRun(
    array $options = [
      'migration' => '',
      'limit' => '',
      'idlist' => '',
      'group' => '',
      'disable-duplicate-check' => FALSE,
    ],
  ) {
    $migrations = $this->orangeDamMigrationDataManager->getMigrationListing();
    // Allow for a user supplied migration option.
    $migrationId = $options['migration'];
    if ($migrationId) {
      if (!$this->orangeDamMigrationDataManager->validateMigrationId($migrationId)) {
        $this->io()->error(dt(
          'The migration ID :migration is not a valid migration.', [
            ':migration' => $migrationId,
          ]));
        return;
      }
    }
    // Allow the user to select the migration interactively.
    else {
      // Prompt the user for a selection and collect the data.
      $migrationId = $this->io()->choice(
        'Migration to run:',
        array_combine(array_keys($migrations), array_keys($migrations)),
      );
    }
    // Inform the user of the action being taken.
    $this->migrationLogger->notice(dt(
        'Running the :migration migration from queued data.', [
          ':migration' => $migrations[$migrationId],
        ]));
    // Run the migration.
    $results = $this->orangeDamMigrationDataManager->runMigrations(
      [$migrationId],
      $options,
      (bool) $options['disable-duplicate-check'],
    );
    // Display the results.
    $this->displayMigrationResults($results);
    $this->displayMigrationQueueListing();
  }

  /**
   * Clear all Orange DAM migration queues.
   *
   * @command orange-dam:migration-clear-queue-all
   * @aliases od-mcqa
   *
   * @usage orange-dam:migration-clear-queue-all
   *   Clear all migration queues.
   */
  public function migrationClearQueueAll() {
    foreach (array_keys($this->orangeDamMigrationDataManager->getQueues()) as $id) {
      $this->queueFactory->get($id)->deleteQueue();
    }
    $this->displayMigrationQueueListing();
    $this->displayUnregisteredQueueCount();
  }

  /**
   * Clear a specific Orange DAM migration queue.
   *
   * @param array $options
   *   The command options.
   *
   * @command orange-dam:migration-clear-queue
   * @aliases od-mcq
   *
   * @option queue
   *   Allows for the queue to be specified.
   *
   * @usage orange-dam:migration-clear-queue
   *   Clear a specific migration queue.
   */
  public function migrationClearQueue(
    array $options = [
      'queue' => '',
    ],
  ) {
    $queue = $options['queue'];
    if ($queue) {
      if (!$this->orangeDamMigrationDataManager->validateQueueName($queue)) {
        $this->io()->error(dt(
          'The queue name :queue is an invalid queue name.', [
            ':queue' => $queue,
          ]));
      }
    }
    else {
      $choices = array_keys($this->orangeDamMigrationDataManager->getQueues());
      $choice = $this->io()->choice('Select the queue to clear:', $choices);
      $queue = $choices[$choice];
    }
    $this->queueLogger
      ->notice(dt('Clearing the %queue queue.', [
        '%queue' => $queue,
      ]));
    $this->queueFactory->get($queue)->deleteQueue();
    $this->displayMigrationQueueListing();
    $this->displayUnregisteredQueueCount();
  }

  /**
   * Provides an overview of migration activity.
   *
   * @param string $timeSince
   *   Full date time string in UTC of any PHP parsable format, such as
   *   "1/25/2023 09:47:01".
   * @param string $timeUntil
   *   Full date time string (in the site's timezone) of any PHP parsable
   *   format, such as "1/25/2023 09:47:01".
   * @param array $options
   *   The command options.
   *
   * @command orange-dam:migration-status
   * @aliases od-ms
   *
   * @options system_id
   *   The system ID from Orange DAM.
   * @options entity_id
   *   The Drupal entity ID.
   * @option limit
   *   The limit of items to display from each migration.
   *
   * @usage orange-dam:migration-status --system-id=EX123 --entity-id=123 --limit=10
   *
   * @usage orange-dam:migration-status
   *   Provides an overview of the migration activity from the queues since a
   *   given time. Defaults to the past 24 hours if no time is provided.
   */
  public function migrationStatus(
    string $timeSince = '',
    string $timeUntil = '',
    array $options = [
      'system-id' => '',
      'entity-id' => '',
      'limit' => '',
    ],
  ) {
    // Set the updated since time, default to 24 hours if no value was provided.
    $timeUpdatedSince = new DrupalDateTime($timeSince);
    if ($timeUpdatedSince->hasErrors()) {
      throw new CommandFailedException('Time since argument ' . escapeshellarg($timeSince) . ' is not a parseable date/time string:' . "\n\t-" . implode("\n\t-", $timeUpdatedSince->getErrors()));
    }
    $timeUpdatedSince->setTimezone(new \DateTimeZone('utc'));
    if ($timeSince === '') {
      $timeUpdatedSince->modify('-24 hours');
    }
    // Set the updated until time, default to now if no value was provided.
    $timeUpdatedUntil = new DrupalDateTime($timeUntil);
    if ($timeUpdatedUntil->hasErrors()) {
      throw new CommandFailedException('Time until argument ' . escapeshellarg($timeUntil) . ' is not a parseable date/time string:' . "\n\t-" . implode("\n\t-", $timeUpdatedUntil->getErrors()));
    }
    $timeUpdatedUntil->setTimezone(new \DateTimeZone('utc'));
    // Display the filtered migration results.
    $this->displayRecentlyMigratedContent(
      $timeUpdatedSince,
      $timeUpdatedUntil,
      $options,
    );
  }

  /**
   * Provides an overview of Orange DAM queues.
   *
   * @command orange-dam:migration-queue-status
   * @aliases od-mqs
   *
   * @usage orange-dam:migration-queue-status
   *   Provides an overview of the sync status of all Orange DAM queues.
   */
  public function migrationQueueStatus() {
    $this->displayMigrationQueueListing();
    $this->displayDeletionQueue();
    $this->displayUnregisteredQueueCount();
  }

  /**
   * Show unregistered queues that lack a corresponding migration.
   *
   * @command orange-dam:migration-missing
   * @aliases od-mm
   *
   * @usage orange-dam:migration-missing
   *   Show unregistered queues that lack a corresponding migration.
   */
  public function migrationMissing() {
    $missing = 0;
    $rows = [];
    $migrations = [];
    $queuesRegistered = $this->orangeDamMigrationDataManager->getQueues();
    // Get a list of migrations.
    foreach ($this->orangeDamMigrationDataManager->getMigrations() as $migration) {
      $migrations[] = $migration->id();
    }
    // Build a table that shows the migration associated with each queue or
    // denote the queues missing a migration.
    foreach ($this->getOrangeDamQueues() as $queue) {
      $migrationNameFromQueue = str_replace('orange_dam_migration:', '', $queue);
      $match = in_array($migrationNameFromQueue, $migrations);
      $registered = array_key_exists($queue, $queuesRegistered);
      if (!$match || !$registered) {
        $missing++;
      }
      $migrationText = $match ? $migrationNameFromQueue : '[missing]';
      $registeredText = $registered ? $queuesRegistered[$queue]['title'] : '[missing]';
      $migrationSuggestion = $match ? 'N/A' : $migrationNameFromQueue;
      $rows[] = [
        $queue,
        $registeredText,
        $migrationText,
        $migrationSuggestion,
      ];
    }
    // Display the results.
    $this->io()->table([
      'Queue Name',
      'Queue Registered',
      'Associated Migration',
      'Suggested Migration ID',
    ], $rows);
    // Display a summary.
    if ($missing > 0) {
      $this->io()->warning(
        $missing . ' queue(s) unregistered or missing a migrations.
        Clearing caches or importing configuration might solve this.
      ');
    }
    else {
      $this->io()->success('All queues are registered and have an associated migration.');
    }
    // Display unregistered queue counts.
    $this->displayUnregisteredQueueCount();
  }

  /**
   * Generate an asset link.
   *
   * @param string $systemIdentifier
   *   The system identifier of the item in Orange DAM. Example: EX1STO431061.
   *
   * @command orange-dam:migration-get-asset-link
   * @aliases od-mgal
   *
   * @usage orange-dam:migration-generate-asset-link EX1STO431061>
   *   Generate an asset link.
   */
  public function migrationGetAssetLink(string $systemIdentifier) {
    try {
      OrangeDamContent::validateSystemIdentifier($systemIdentifier);
    }
    catch (\Exception $e) {
      $this->migrationLogger->error($e->getMessage());
      return;
    }
    if ($url = $this->orangeDamUpdatedDataManager->getAssetUrl($systemIdentifier)) {
      $this->io()->text($url);
    }
  }

  /**
   * Get subtitles for an item.
   *
   * @param string $systemIdentifier
   *   The system identifier of the item in Orange DAM. Example: EX1STO431061.
   *
   * @command orange-dam:migration-get-subtitles
   * @aliases od-mgs
   *
   * @usage orange-dam:migration-get-subtitles EX1STO431061
   *   Generate an asset link.
   */
  public function migrationSubtitles(string $systemIdentifier) {
    try {
      OrangeDamContent::validateSystemIdentifier($systemIdentifier);
    }
    catch (\Exception $e) {
      $this->migrationLogger->error($e->getMessage());
      return;
    }
    if (!$item = $this->orangeDamApi->getContentItem($systemIdentifier)) {
      $this->io()->error($this->t(
        'Unable to retrieve an item with system ID ":id".', [
          ':id' => $systemIdentifier,
        ]));
      return;
    }
    if ($subtitles = $this->orangeDamApi->getSubtitles($item->data()->RecordID)) {
      $this->io()->text(print_r($subtitles, TRUE));
    }
    else {
      $this->io()->error($this->t(
        'Unable to retrieve subtitles for the item with system ID ":id".', [
          ':id' => $systemIdentifier,
        ]));
    }
  }

  /**
   * Process the deletion queue.
   *
   * @param string $limit
   *   The maximum number of items to process.
   *
   * @command orange-dam:migration-process-deletion-queue
   * @aliases od-mpdq
   *
   * @usage orange-dam:migration-process-deletion-queue
   *   Process all items in the deletion queue.
   * @usage orange-dam:migration-process-deletion-queue 123
   *   Process 123 items in the deletion queue.
   */
  public function migrationProcessDeletionQueue(string $limit = '') {
    $limit = $limit ?: 0;
    $this->orangeDamMigrationDataManager->processDeletionQueue((int) $limit);
  }

  /**
   * List all available Orange DAM fields.
   *
   * @command orange-dam:api-list-fields
   * @aliases od-alf
   *
   * @usage orange-dam:api-list-fields
   *   Lists all the available Orange DAM fields.
   */
  public function apiListFields() {
    $rows = [];
    foreach ($this->orangeDamApi->listFields() as $fieldId => $fieldName) {
      // Abbreviate the field name if necessary.
      if (strlen($fieldName) > static::MAX_FIELD_CHARACTER_LENGTH) {
        $fieldName = sprintf('%s…',
          strtok(wordwrap($fieldName, static::MAX_FIELD_CHARACTER_LENGTH), PHP_EOL)
        );
      }
      // Build the row data.
      $rows[] = [
        $fieldId,
        str_replace(PHP_EOL, ' ', $fieldName),
      ];
    }
    $this->io()->table(['Field ID', 'Field Name'], $rows);
  }

  /**
   * List all available Search API criteria fields from Orange DAM.
   *
   * @command orange-dam:api-list-search-criteria
   * @aliases od-alsc
   *
   * @usage orange-dam:api-list-search-criteria
   *   The available Search API criteria fields from Orange DAM.
   */
  public function apiListSearchCriteria() {
    $rows = [];
    foreach ($this->orangeDamApi->listSearchCriteria() as $field) {
      $fieldName = $field?->Name;
      // Abbreviate the field name if necessary.
      if (strlen($fieldName) > static::MAX_FIELD_CHARACTER_LENGTH) {
        $fieldName = sprintf('%s…',
          strtok(wordwrap($fieldName, static::MAX_FIELD_CHARACTER_LENGTH), PHP_EOL)
        );
      }
      // Build the row data.
      $rows[] = [
        $fieldName,
        isset($field->Description) ? wordwrap($field->Description) : 'N/A',
      ];
    }
    $this->io()->table(['Name', 'Description'], $rows);
  }

  /**
   * Array sorting function for queue listing reports.
   *
   * @param array $a
   *   The first item to compare.
   * @param array $b
   *   The second item to compare.
   *
   * @return int
   *   Integer indicating sort order.
   */
  public static function sortQueues(array $a, array $b): int {
    return strtolower($a[0]) <=> strtolower($b[0]);
  }

  /**
   * An array of queue names that have the Orange DAM prefix in their name.
   *
   * @return string[]
   *   Orange DAM prefixed queue names.
   */
  protected function getOrangeDamQueues(): array {
    return $this->databaseConnection
      ->select('queue', 'q')
      ->fields('q', ['name'])
      ->condition('name', $this->databaseConnection->escapeLike('orange_dam_migration') . '%', 'LIKE')
      ->distinct()
      ->execute()
      ->fetchCol();
  }

  /**
   * Get an array of unregistered queue names.
   *
   * @return string[]
   *   An array of unregistered queue names.
   */
  protected function getUnregisteredQueueNames(): array {
    $allQueues = $this->getOrangeDamQueues();
    $registeredQueues = array_keys($this->orangeDamMigrationDataManager->getQueues());
    return array_diff($allQueues, $registeredQueues);
  }

  /**
   * Get the number of items in an unregistered queue by name.
   *
   * @param string $queueName
   *   The queue name.
   *
   * @return int
   *   The number of items in the queue.
   */
  protected function getQueueItemCount(string $queueName): int {
    return $this->databaseConnection
      ->select('queue', 'q')
      ->fields('q', ['name'])
      ->condition('name', $queueName)
      ->countQuery()->execute()->fetchField();
  }

  /**
   * Display a table of all items in unregistered queues.
   */
  protected function displayUnregisteredQueueCount(): void {
    $rows = [];
    $unregisteredQueues = $this->getUnregisteredQueueNames();
    if ($unregisteredQueues === []) {
      $this->io()->success('No unregistered queue items.');
      return;
    }
    foreach ($unregisteredQueues as $unregisteredQueue) {
      $rows[] = [
        $unregisteredQueue,
        $this->getQueueItemCount($unregisteredQueue),
      ];
    }
    $this->io()->title('Unregistered Queue Items');
    $this->io()->table(['Queue Name', 'Count'], $rows);
  }

  /**
   * Display the recently migrated content table.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $timeUpdatedSince
   *   The time the migration was run since.
   * @param \Drupal\Core\Datetime\DrupalDateTime $timeUpdatedUntil
   *   The time the migration was run until.
   * @param array $options
   *   Supported options:
   *     - system-id: The System ID from Orange DAM to filter results by.
   *     - entity-id: The Drupal entity ID to filter results by.
   *     - limit: The maximum items from each migration to display.
   *   Additional options may be provided and used by other modules that hook
   *   into this and listen to the TK event.
   */
  protected function displayRecentlyMigratedContent(
    DrupalDateTime $timeUpdatedSince,
    DrupalDateTime $timeUpdatedUntil,
    array $options,
  ): void {
    // Show the recently migrated items.
    $this->io()->title(dt('Content Migrated Since :timeSince to :timeUntil.', [
      ':timeSince' => $timeUpdatedSince->setTimezone(new \DateTimeZone('utc'))->format('c'),
      ':timeUntil' => $timeUpdatedUntil->setTimezone(new \DateTimeZone('utc'))->format('c'),
    ]));
    $rows = [];
    $migrations = $this->orangeDamMigrationDataManager->getMigrations();
    foreach ($migrations as $migration) {
      $tableName = sprintf('migrate_map_%s', $migration->id());
      // Verify that the mapping table exists.
      if (!$this->databaseConnection->schema()->tableExists($tableName)) {
        continue;
      }
      // Find items in the mapping table.
      $query = $this->databaseConnection
        ->select($tableName, 'mm')
        ->fields('mm')
        ->where('last_imported > :time_since', [
          ':time_since' => $timeUpdatedSince->format('U'),
        ])
        ->where('last_imported < :time_until', [
          ':time_until' => $timeUpdatedUntil->format('U'),
        ]);
      // Add optional filters if a value was provided.
      if (!empty($options['system-id'])) {
        $query->where('mm.sourceid1 = :system_id', [
          ':system_id' => $options['system-id'],
        ]);
      }
      if (!empty($options['entity-id'])) {
        $query->where('mm.destid1 = :entity_id', [
          'entity_id' => $options['entity-id'],
        ]);
      }
      if (!empty($options['limit'])) {
        $query->range(0, $options['limit']);
      }
      // Allow other modules to alter the query.
      $this->eventDispatcher->dispatch(
        new OrangeDamStatusQueryEvent($options, $query),
        OrangeDamStatusEvents::ALTER_QUERY,
      );
      // Run the query and display the results.
      $results = $query->execute()->fetchAll();
      foreach ($results as $result) {
        $lastImported = DrupalDateTime::createFromTimestamp($result->last_imported);
        // Get the entity type and load the entity.
        // @todo Address upstream the fact that getDerivativeId() is not
        //   guaranteed to exist on destination plugins.
        // @see https://www.drupal.org/project/drupal/issues/2783715
        $entityType = $migration->getDestinationPlugin()->getDerivativeId();
        $entity = NULL;
        if (!empty($result->destid1)) {
          $entity = $this->entityTypeManager
            ->getStorage($entityType)
            ->load($result->destid1);
        }
        if (!$entity) {
          $this->io()->warning(dt('Unable to find the entity with Source ID :source_id and destination ID :destination_id.', [
            ':source_id' => $result->sourceid1,
            ':destination_id' => $result->destid1,
          ]));
        }
        // Get the bundle for the item.
        $bundle = $entity ? $entity->bundle() : 'unknown';
        // Build the title and abbreviate it if necessary.
        $title = $entity ? $entity->label() : 'Unknown Title';
        if (strlen($title) > static::MAX_TITLE_CHARACTER_LENGTH) {
          $title = sprintf('%s…', strtok(wordwrap($title, static::MAX_TITLE_CHARACTER_LENGTH), PHP_EOL));
        }
        // Build the table row.
        $row = [
          $result->sourceid1,
          sprintf('%s:%s', $entityType, $bundle),
          $result->destid1,
          $lastImported->setTimezone(new \DateTimeZone('utc'))->format('c'),
          $title,
        ];
        // Allow other modules to alter the row.
        /** @var \Drupal\orange_dam\Event\OrangeDamStatusRowEvent $rowEvent */
        $rowEvent = $this->eventDispatcher->dispatch(
          new OrangeDamStatusRowEvent($row, $result, $entity),
          OrangeDamStatusEvents::ALTER_ROW,
        );
        $rows[] = $rowEvent->getRow();
      }
    }
    $headers = [
      'System ID',
      'Entity Type',
      'Entity ID',
      'Last Updated',
      'Title',
    ];
    // Allow other modules to alter the table headers.
    /** @var \Drupal\orange_dam\Event\OrangeDamStatusHeaderEvent $headerEvent */
    $headerEvent = $this->eventDispatcher->dispatch(
      new OrangeDamStatusHeaderEvent($headers),
      OrangeDamStatusEvents::ALTER_HEADER,
    );
    $this->io()->table($headerEvent->getHeaders(), $rows);
  }

  /**
   * Display the migration queue listing table.
   *
   * @param string[] $tags
   *   The array of migration tags to filter the results by.
   */
  protected function displayMigrationQueueListing(array $tags = []): void {
    // Show the size of each migration queue.
    $this->io()->title('Migration Queues');
    $headers = ['Queue Name', 'Items', 'Migration'];
    $rows = [];
    $queues = $this->orangeDamMigrationDataManager->getQueues();
    foreach ($this->orangeDamMigrationDataManager->getMigrations() as $migration) {
      // If tags were supplied, only add rows that match one of the tags.
      if ($tags && !array_intersect($tags, $migration->getMigrationTags())) {
        continue;
      }
      // Build each row.
      $configuration = $migration->getSourceConfiguration();
      $queueId = $configuration['queue_name'];
      // Alert the user if the queue was not found.
      if (!isset($queues[$queueId])) {
        $this->io()->error(
          dt('The queue ":queue" is missing or not registered. Create a migration with "id: :queue" to register it.', [
            ':queue' => $queueId,
          ]));
        continue;
      }
      $rows[] = [
        $queues[$queueId]['title'],
        number_format($this->queueFactory->get($queueId)->numberOfItems()),
        $migration->id(),
      ];
    }
    usort($rows, [OrangeDamCommands::class, 'sortQueues']);
    $this->io()->table($headers, $rows);
  }

  /**
   * Display information about the deletion queue.
   */
  protected function displayDeletionQueue() {
    $rows = [];
    // Deletion Queue.
    $this->io()->title('Deletion Queue');
    $deletionQueue = $this->orangeDamMigrationDataManager->getDeletionQueue();
    $rows[] = [
      $this->orangeDamMigrationDataManager::DELETION_QUEUE,
      number_format($deletionQueue->numberOfItems()),
    ];
    $this->io()->table(['Queue Name', 'Items'], $rows);
  }

  /**
   * Display migration results.
   *
   * @param array $results
   *   The results from $this->orangeDamMigrationDataManager->runMigrations.
   */
  protected function displayMigrationResults(array $results) {
    // Warn the user if nothing was run.
    if ($results === []) {
      $this->migrationLogger
        ->warning(dt(
          'No migrations were run. Ensure that the migrations exist and are tagged with "orange_dam".'
        ));
    }
    // Record the results of the migrations.
    $headers = array_keys($results[0]);
    $headers = array_map('ucwords', $headers);
    $this->io()->table($headers, $results);
  }

  /**
   * Create a date time object while validating the source string format.
   *
   * @param string $time
   *   The date time string.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The date time object.
   *
   * @throws \Exception
   */
  protected function createDateTime(string $time): DrupalDateTime {
    try {
      $dateTime = new DrupalDateTime($time);
      $dateTime->setTimezone(new \DateTimeZone('utc'));
    }
    catch (\Exception) {
      throw new \Exception(sprintf(
        'Invalid source date format "%s".',
        $time,
      ));
    }
    return $dateTime;
  }

}
