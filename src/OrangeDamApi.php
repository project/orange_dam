<?php

namespace Drupal\orange_dam;

use Chromatic\OrangeDam\DataTableIdentifiers;
use Chromatic\OrangeDam\Endpoints\DataTable;
use Chromatic\OrangeDam\Endpoints\Endpoint;
use Chromatic\OrangeDam\Endpoints\ObjectManagement;
use Chromatic\OrangeDam\Endpoints\Search;
use Chromatic\OrangeDam\Factory as OrangeDamConnection;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\orange_dam\Event\OrangeDamApiRequestEvent;
use Drupal\orange_dam\Event\OrangeDamApiRequestEvents;
use Drupal\orange_dam\Event\OrangeDamApiResponseEvents;
use Drupal\orange_dam\Event\OrangeDamApiResponseReceivedEvent;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Orange DAM API interaction manager.
 */
class OrangeDamApi {

  use StringTranslationTrait;

  /**
   * Number of seconds to sleep before re-trying a failed request.
   */
  public const API_RETRY_SLEEP = 2;

  /**
   * The oauth expire time state key.
   */
  public const OAUTH_EXPIRE_IN = 'orange_dam.expires_in';

  /**
   * The default API request timeout in seconds.
   */
  public const DEFAULT_API_REQUEST_TIMEOUT = 30;

  /**
   * HTTP status code for OK.
   */
  public const HTTP_STATUS_OK = 200;

  /**
   * The index of the first page for API results.
   */
  public const INITIAL_PAGE_NUMBER = 1;

  /**
   * An array of runtime configuration overrides for use in the API connection.
   *
   * This can help with testing from a server with an un-writable filesystem.
   */
  protected array $configOverrides = [];

  /**
   * Content Types which require DataTable API data.
   */
  protected array $dataTableContentTypes = [];

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentRequest;

  /**
   * The Orange DAM connection.
   *
   * @var \Chromatic\OrangeDam\Factory|null
   */
  protected OrangeDamConnection|null $orangeDamConnection = NULL;

  /**
   * Constructor.
   */
  public function __construct(
    protected LoggerChannelInterface $logger,
    protected ClientInterface $httpClient,
    protected ConfigFactoryInterface $configFactory,
    protected StateInterface $state,
    RequestStack $requestStack,
    protected TimeInterface $time,
    protected EventDispatcherInterface $eventDispatcher,
    protected OrangeDamMigrationDataManager $migrationDataManager,
    protected OrangeDamConfigurationManager $orangeDamConfigurationManager,
  ) {
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * Repeatedly call an endpoint until success, timeout, or max retries reached.
   *
   * @param \Chromatic\OrangeDam\Endpoints\Endpoint $endpoint
   *   The endpoint to call.
   * @param string $method
   *   The method to call on the endpoint.
   * @param array $arguments
   *   An array of arguments to the endpoint method, either ordered or as named
   *   parameters.
   * @param callable $returncall
   *   Closure that, when given the Response, returns the part of the response
   *   that the caller wishes to return from this function.
   */
  public function callEndpoint(Endpoint $endpoint, string $method, array $arguments, callable $returncall): mixed {
    // Attempt the request.
    $attempts = 0;
    // Get the maximum attempts configuration.
    $maxAttempts = $this->configFactory->get('orange_dam.settings')->get('api_max_retry_attempts');
    if (empty($maxAttempts)) {
      throw new \Exception('Missing Orange DAM Maximum API Retry Attempts ("orange_dam.settings api_max_retry_attempts") configuration.');
    }
    do {
      $attempts++;
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $endpoint->$method(...$arguments);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
      }
      catch (ConnectException $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        $this->logger->warning(
          'An Orange DAM API timeout exception occurred when calling :call with arguments ":args". Retrying the API request.',
          [':call' => get_class($endpoint) . '->' . $method, ':args' => json_encode($arguments)],
        );
        // Retry the request.
        continue;
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if ($response->isEmpty()) {
        throw new \Exception(sprintf(
          'Unexpected Orange DAM API response. Received %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      $return_value = $returncall($response);
      if (!isset($return_value)) {
        $this->logger->error(
          'Unable to retrieve return value for :call with arguments :args',
          [':call' => get_class($endpoint) . '->' . $method, ':args' => json_encode($arguments)],
        );
        return FALSE;
      }
      return $return_value;
    } while ($attempts <= $maxAttempts);
    // If it still failed after multiple attempts, log an error.
    $this->logger->error(
      'Giving up on calling :call after :attempts attempts. Orange DAM API timed out after :timeout seconds.',
      [
        ':call' => get_class($endpoint) . '->' . $method,
        ':attempts' => $maxAttempts,
        ':timeout' => $this->getRequestTimeout(),
      ],
    );
    return FALSE;
  }

  /**
   * Provide a means to reliably override the installed API configuration.
   *
   * Configuration overrides provided from a ConfigFactoryOverrideInterface
   * cannot have a higher priority than settings files, meaning there is no way
   * to dynamically override the site configuration for testing.
   */
  public function configOverrides(array $overrides): void {
    foreach ($overrides as $setting => $value) {
      $this->configOverrides[$setting] = $value;
    }
  }

  /**
   * Get the list of available Orange DAM field names.
   *
   * @return array
   *   An array of Orange DAM field names keyed by the field machine name.
   *
   * @throws \Exception
   */
  public function listFields(): array {
    $fields = [];
    $params = [
      'format' => 'json',
    ];
    try {
      /** @var \Chromatic\OrangeDam\Http\Response $response */
      $response = $this->orangeDamConnection()->search()->listFields($params);
      $this->eventDispatcher->dispatch(
        new OrangeDamApiResponseReceivedEvent($response),
        OrangeDamApiResponseEvents::RECEIVED
      );
      $content = $response->getData();
    }
    catch (\Exception $e) {
      $variables = Error::decodeException($e);
      $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }
    // Validate the response.
    if (empty($content)) {
      throw new \Exception(sprintf(
        'Unexpected Orange DAM API response. Received %s with message "%s."',
        $response->getStatusCode(),
        $response->getReasonPhrase(),
      ));
    }
    // Validate that items were found.
    if (empty($content->APIResponse->Metadata)) {
      $this->logger->error($this->t('No Orange DAM field information found.'));
      return $fields;
    }
    return (array) $content->APIResponse->Metadata;
  }

  /**
   * Get the list of available Search API criteria fields from Orange DAM.
   *
   * @return array
   *   An array of available Orange DAM search criteria fields.
   *
   * @throws \Exception
   */
  public function listSearchCriteria(): array {
    $fields = [];
    $params = [
      'format' => 'json',
    ];
    try {
      /** @var \Chromatic\OrangeDam\Http\Response $response */
      $response = $this->orangeDamConnection()->search()->listCriteria($params);
      $this->eventDispatcher->dispatch(
        new OrangeDamApiResponseReceivedEvent($response),
        OrangeDamApiResponseEvents::RECEIVED
      );
      $content = $response->getData();
    }
    catch (\Exception $e) {
      $variables = Error::decodeException($e);
      $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }
    // Validate the response.
    if (empty($content)) {
      throw new \Exception(sprintf(
        'Unexpected Orange DAM API response. Received %s with message "%s."',
        $response->getStatusCode(),
        $response->getReasonPhrase(),
      ));
    }
    // Validate that items were found.
    if (empty($content->APIResponse)) {
      $this->logger->error($this->t('No Orange DAM search criteria information found.'));
      return $fields;
    }
    return (array) $content->APIResponse;
  }

  /**
   * Retrieves all data about an item from the DataTable API.
   *
   * @param string $identifier
   *   The identifier for the item.
   * @param \Chromatic\OrangeDam\DataTableIdentifiers $identifierType
   *   The identifier type.
   *
   * @return object|null
   *   The Orange DAM item data.
   *
   * @throws \Exception
   *   On http error or unexpected Orange DAM API response.
   */
  public function getDataTableData(string $identifier, DataTableIdentifiers $identifierType):?object {
    // Build the request parameters.
    $params = [
      $identifierType->value => $identifier,
      'format' => 'json',
    ];
    try {
      /** @var \Chromatic\OrangeDam\Http\Response $response */
      $response = $this->orangeDamConnection()->dataTable()->getDocumentData($params);
      $this->eventDispatcher->dispatch(
        new OrangeDamApiResponseReceivedEvent($response),
        OrangeDamApiResponseEvents::RECEIVED
      );
      $content = $response->getData();
    }
    catch (\Exception $e) {
      $variables = Error::decodeException($e);
      $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }
    // Validate the response.
    if (empty($content)) {
      throw new \Exception(sprintf(
        'Unexpected Orange DAM API response. Received %s with message "%s."',
        $response->getStatusCode(),
        $response->getReasonPhrase(),
      ));
    }
    // Log an error if no item with the ID was found.
    if (empty($content->Response[0])) {
      $this->logger->warning('Empty Orange DAM DataTable response for item ":id".', [
        ':id' => $identifier,
      ]);
      return NULL;
    }

    return $content->Response[0];
  }

  /**
   * Retrieves data from the Orange DAM Search API for an item.
   *
   * @param string $identifier
   *   The System Identifier of the item in Orange DAM. Example: EX1STO431061.
   * @param array $fields
   *   The optional list of fields to include. Defaults to the config value.
   *
   * @return object|null
   *   The Orange DAM item data.
   *
   * @throws \Exception
   *   On http/api responses or unexpected data.
   */
  public function getSearchApiData(string $identifier, array $fields = []):?object {
    $fields = $fields === [] ? $this->getSearchFieldNamesCommaSeparated() : implode(',', $fields);
    // Build the request parameters.
    $params = [
      'query' => [
        'SystemIdentifier:' . $identifier,
      ],
      'fields' => $fields,
      'format' => 'json',
    ];
    /** @var \Drupal\orange_dam\Event\OrangeDamApiRequestEvent $orangeDamApiEvent */
    $orangeDamApiEvent = $this->eventDispatcher->dispatch(
      new OrangeDamApiRequestEvent($params),
      OrangeDamApiRequestEvents::ITEM_DATA
    );
    $params = $orangeDamApiEvent->getParameters();
    // Combine the query parameters into a string.
    $params['query'] = implode(' ', $params['query']);
    try {
      /** @var \Chromatic\OrangeDam\Http\Response $response */
      $response = $this->orangeDamConnection()->search()->search($params);
      $this->eventDispatcher->dispatch(
        new OrangeDamApiResponseReceivedEvent($response),
        OrangeDamApiResponseEvents::RECEIVED
      );
      $content = $response->getData();
    }
    catch (\Exception $e) {
      $variables = Error::decodeException($e);
      $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }
    // Validate the response.
    if (empty($content)) {
      throw new \Exception(sprintf(
        'Unexpected Orange DAM API response. Received %s with message "%s."',
        $response->getStatusCode(),
        $response->getReasonPhrase(),
      ));
    }
    // Log an error if no item with the ID was found.
    if (empty($content->APIResponse->Items[0])) {
      $this->logger->warning('Unable to retrieve information for item ":id" from Orange DAM Search API.', [
        ':id' => $identifier,
      ]);
      return NULL;
    }
    return $content->APIResponse->Items[0];
  }

  /**
   * Retrieves & builds an Orange Dam content item from the Orange DAM API.
   *
   * @param string $systemIdentifier
   *   The System Identifier of the item in Orange DAM. Example: EX1STO431061.
   *
   * @return \Drupal\orange_dam\OrangeDamItemInterface|false
   *   The Orange DAM content item or false on failure.
   */
  public function getContentItem(string $systemIdentifier): OrangeDamItemInterface|false {
    if (!$data = $this->getSearchApiData($systemIdentifier)) {
      return FALSE;
    }
    // Build and return an Orange DAM item.
    return $this->buildOrangeDamContentItem($data);
  }

  /**
   * Queues an OrangeDam item.
   *
   * If you pass an already existing Orange Dam content item, the item will be
   * queued. If you pass a system identifier, the content item will first be
   * retrieved, and then queued.
   *
   * @param string|\Drupal\orange_dam\OrangeDamItemInterface $item
   *   The System Identifier of the item in Orange DAM (Example: EX1STO431061)
   *   or an already retrieved Orange Dam content item.
   *
   * @return \Drupal\orange_dam\OrangeDamItemInterface|false
   *   The Orange DAM content item or false on failure.
   */
  public function queueContentItem(string|OrangeDamItemInterface $item): OrangeDamItemInterface|false {
    if (is_string($item)) {
      $item = $this->getContentItem($item);
    }
    if ($item instanceof OrangeDamItemInterface) {
      // Queue the item.
      $this->migrationDataManager->queueMigrationItem($item);
    }
    return $item;
  }

  /**
   * Retrieves & queues updated items in the Orange DAM API since a given time.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeUpdatedSince
   *   The time to check for items changed since.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeUpdatedUntil
   *   The time to check for items changed until.
   * @param array $contentTypes
   *   The content types to filter the results by. Defaults to all.
   * @param int $limit
   *   The maximum number of items, rounded up to the nearest page size.
   * @param int $pageNumber
   *   The initial page number to request.
   * @param string $sort
   *   An Orange Logic API sort option.
   *
   * @return int
   *   The number of Orange DAM items in the API result.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   * @throws \Exception
   */
  public function queueContent(
    ?DrupalDateTime $timeUpdatedSince,
    ?DrupalDateTime $timeUpdatedUntil,
    array $contentTypes = [],
    int $limit = 0,
    int $pageNumber = self::INITIAL_PAGE_NUMBER,
    string $sort = '',
  ):int {
    $queued = 0;
    $times = [];
    $quit = FALSE;
    $nextPage = FALSE;
    $progressBar = new ProgressBar(new ConsoleOutput());
    $itemsPerPage = $this->configFactory->get('orange_dam.settings')->get('api_items_per_page_search');
    // Validate the items per page value.
    if (empty($itemsPerPage)) {
      $this->logger->error('The :config config value cannot be "0" or empty.', [
        ':config' => 'api_items_per_page_search',
      ]);
      throw new ConfigValueException('The value of Orange Dam configuration key api_items_per_page_search must be a positive integer greater than zero.');
    }
    $contentTypesFilter = $contentTypes ?: $this->orangeDamConfigurationManager->getContentTypeNames();
    // Build the request parameters.
    $params = [
      'countperpage' => $itemsPerPage,
      'fields' => $this->getSearchFieldNamesCommaSeparated(),
      'format' => 'json',
      'pagenumber' => $pageNumber,
      'query' => [],
    ];
    if ($sort) {
      $params['sort'] = $sort;
    }
    array_walk($contentTypesFilter, fn(&$x) => $x = "'$x'");
    $params['query']['types'] = sprintf('DocSubType:(%s)', implode(' OR ', $contentTypesFilter));
    if ($timeUpdatedSince) {
      $params['query']['since'] = sprintf('EditDate>:%s', $timeUpdatedSince->format(Search::DATE_FORMAT));
    }
    if ($timeUpdatedUntil) {
      $params['query']['until'] = sprintf('EditDate<:%s', $timeUpdatedUntil->format(Search::DATE_FORMAT));
    }
    // Allow other modules to alter the query parameters.
    /** @var \Drupal\orange_dam\Event\OrangeDamApiRequestEvent $orangeDamApiEvent */
    $orangeDamApiEvent = $this->eventDispatcher->dispatch(new OrangeDamApiRequestEvent($params), OrangeDamApiRequestEvents::UPDATED_ITEMS);
    $params = $orangeDamApiEvent->getParameters();
    if (!empty($params['query']['since'])) {
      $this->logger->notice(
        'Requesting Orange DAM content updated since @time.',
        ['@time' => substr($params['query']['since'], 10)]
      );
    }
    // Combine the query parameters into a string.
    $params['query'] = implode(' ', $params['query']);
    // If a limit was supplied, log the type and limit parameters used.
    if ($limit) {
      $this->logger->notice(
        'A retrieval limit of :limit items of type :type was set.', [
          ':limit' => $limit,
          ':type' => implode(', ', $contentTypesFilter),
        ]
      );
    }
    // Repeat the request until all items are retrieved.
    do {
      $requestTimeBefore = new DrupalDateTime();
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()->search()->search($params);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (ConnectException $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        $this->logger->warning(
          'An Orange DAM API connection exception occurred when requesting page :page. Retrying the API request.', [
            ':page' => $params['pagenumber'],
          ]);
        // Retry the request after sleeping a bit.
        sleep(static::API_RETRY_SLEEP);
        continue;
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      $requestTimeAfter = new DrupalDateTime();
      // Validate the response.
      if (empty($content)) {
        $this->logger->error(
          'Empty Orange DAM API response. Received status %status: "%reason."',
          [
            '%status' => $response->getStatusCode(),
            '%reason' => $response->getReasonPhrase(),
          ]
        );
        // @todo Consider a custom OrangeDamApiException.
        throw new \Exception(sprintf(
          'Empty Orange DAM API response. Received status %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      // Handle empty result sets.
      if (empty($content->APIResponse->Items)) {
        if ($timeUpdatedSince) {
          if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
            $this->logger->debug(
              'No Orange DAM content updated since %updated_time.',
              ['%updated_time' => $timeUpdatedSince->setTimezone(new \DateTimeZone('utc'))->format('c')]
            );
          }
        }
        if ($contentTypes) {
          $this->logger->warning('No Orange DAM items of type :type found.', [
            ':type' => implode(', ', $contentTypes),
          ]);
        }
        return $queued;
      }
      // Configure the progress bar on the first iteration of the loop.
      if (!$nextPage) {
        $totalCount = $content->APIResponse->GlobalInfo->TotalCount;
        // Page numbering starts at 1, so we need to subtract 1 to ensure that
        // starting at the beginning doesn't throw off this calculation.
        $steps = (int) ceil($totalCount / $itemsPerPage) - ($pageNumber - 1);
        $this->logger->notice(
          'Starting at page :page, found :count item(s) of type :type in the Orange Logic Search API results.', [
            ':page' => $pageNumber,
            ':count' => number_format($totalCount),
            ':type' => implode(', ', $contentTypesFilter),
          ]);
        $progressBar->setMaxSteps($steps);
        $progressBar->start();
      }
      // Queue the items for migration.
      foreach ($content->APIResponse->Items as $item) {
        // Check if we have met the limit if one was provided.
        $quit = $limit && $queued >= $limit;
        if ($quit) {
          break;
        }
        // Build the Orange DAM content item and queue it.
        $this->migrationDataManager->queueMigrationItem(
          $this->buildOrangeDamContentItem($item),
        );
        $queued++;
      }
      // Record the elapsed request time.
      $elapsedTime = $requestTimeBefore->diff($requestTimeAfter)->s;
      $times[] = $elapsedTime;
      // Determine if we need to request the next page and increment the pager.
      $nextPage = isset($content->APIResponse->GlobalInfo->NextPage);
      $params['pagenumber']++;
      // Update the progress bar.
      $progressBar->advance();
      // Close things out if we are done.
      if (!$nextPage || $quit) {
        $progressBar->finish();
        $progressBar->clear();
      }
      if ($quit) {
        break;
      }
    } while ($nextPage);
    // Log the average request time if requests were made successfully.
    if ($times !== []) {
      // Log the average request time.
      $this->logger->info('Average API request time: :time seconds.', [
        ':time' => array_sum($times) / count($times),
      ]);
    }
    // If all our assumptions are correct, the following if statement will never
    // evaluate as true (either because an exception occurred above, or, if no
    // exception occurred, the $queued and $totalCount will always be the same).
    // We add this log output to prove ourselves wrong.
    if (isset($totalCount) && $totalCount && $totalCount != $queued) {
      $this->logger->error(
        "Queued item count (:queued) does not match count of items (:total) found!",
        [':queued' => $queued, ':total' => $totalCount],
      );
      // @todo Possibly throw an Exception here someday.
    }

    return $queued;
  }

  /**
   * Get an asset file.
   *
   * @param string $systemIdentifier
   *   The System Identifier of the item in Orange DAM. Example: EX1STO431061.
   * @param string $assetFormat
   *   The asset format to use such as "TRX" or "TR1" as defined by Orange DAM.
   */
  public function getAssetFile(string $systemIdentifier, string $assetFormat): OrangeDamFileStreamInterface|false {
    /** @var \Chromatic\OrangeDam\Http\Response $response */
    $response = $this->callEndpoint(
      endpoint: $this->orangeDamConnection()->assetFile(),
      method: 'getFormatFile',
      arguments:[
        'format' => $assetFormat,
        'queryConditions' => [
          'SystemIdentifier' => $systemIdentifier,
        ],
      ],
      returncall: function ($response) {
        return $response;
      }
    );
    if ($size = $response->getBody()->getSize()) {
      if ($types = $response->getHeader('content-type')) {
        $type = $types[array_key_last($types)];
        if ($mimetype = current(explode(';', $type))) {
          return new OrangeDamFileStream($mimetype, $size, $response->getBody());
        }
      }
    }
    return FALSE;
  }

  /**
   * Generate an asset URL.
   *
   * @param string $systemIdentifier
   *   The System Identifier of the item in Orange DAM. Example: EX1STO431061.
   * @param string $assetFormat
   *   The asset format to use such as "TRX" or "TR1" as defined by Orange DAM.
   *
   * @return string|false
   *   The asset URL or false if there was a problem.
   *
   * @see https://EXAMPLESUBDOMAIN.orangelogic.com/swagger/ui/index#!/AssetLink/APIService_AssetLink_v1_0_CreateFormatLink
   */
  public function getAssetUrl(string $systemIdentifier, string $assetFormat): string|false {
    // The Orange Logic API default behavior is for the asset URL to be tied to
    // the version set at time of the request. We keep that default behavior
    // here, but supplement it with an override so that the asset url can
    // continually follow the latest version of the asset instead.
    $stickToCurrentVersion = $this->configFactory->get('orange_dam.settings')->get('asset_url_follow_latest') ? 'false' : 'true';
    // Build and send the request.
    $params = [
      'Identifier' => $systemIdentifier,
      'StickToCurrentVersion' => $stickToCurrentVersion,
      'LogAccess' => 'true',
      'CreateDownloadLink' => 'false',
      'AssetFormat' => $assetFormat,
      'format' => 'json',
    ];
    $attempts = 0;
    // Get the maximum attempts configuration.
    $maxAttempts = $this->configFactory->get('orange_dam.settings')->get('api_max_retry_attempts');
    if (empty($maxAttempts)) {
      throw new \Exception('Missing Orange DAM Maximum API Retry Attempts ("orange_dam.settings api_max_retry_attempts") configuration.');
    }
    do {
      $attempts++;
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()->assetLink()->createFormatLink($params);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (ConnectException $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        $this->logger->warning(
          'An Orange DAM API timeout exception occurred when requesting the asset URL for item ":id" with the asset format :format. Retrying the API request.', [
            ':id' => $systemIdentifier,
            ':format' => $assetFormat,
          ]);
        // Retry the request.
        continue;
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if (empty($content)) {
        throw new \Exception(sprintf(
          'Unexpected Orange DAM API response. Received %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      if (!isset($content->APIResponse->Response->Link)) {
        $this->logger->error('Unable to retrieve Orange DAM asset URL data for item :item with the asset format ":format".', [
          ':item' => $systemIdentifier,
          ':format' => $assetFormat,
        ]);
        return FALSE;
      }
      return $content->APIResponse->Response->Link;
    } while ($attempts <= $maxAttempts);
    // If it still failed after multiple attempts, log an error.
    $this->logger->error(
      'Giving up on retrieving the asset URL for item :item with format ":format" after :attempts attempts. Orange DAM API timed out after :timeout seconds.', [
        ':attempts' => $maxAttempts,
        ':item' => $systemIdentifier,
        ':format' => $assetFormat,
        ':timeout' => $this->getRequestTimeout(),
      ]);
    return FALSE;
  }

  /**
   * Gets and queues all deleted items from the Orange DAM.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $timeDeletedSince
   *   The time to check for items deleted since.
   * @param \Drupal\Core\Datetime\DrupalDateTime $timeDeletedUntil
   *   The time to check for items deleted until. Defaults to the current time.
   *
   * @return int
   *   The number of items queued for deletion.
   *
   * @throws \Exception
   */
  public function getDeletedContent(
    DrupalDateTime $timeDeletedSince,
    DrupalDateTime $timeDeletedUntil = new DrupalDateTime(),
  ): int {
    $queued = 0;
    $progressBar = new ProgressBar(new ConsoleOutput());
    $formattedDeletedSince = $timeDeletedSince->format(ObjectManagement::DATE_FORMAT);
    // Build the request parameters.
    $params = [
      'query' => [
        'From' => $formattedDeletedSince,
        'To' => $timeDeletedUntil->format(ObjectManagement::DATE_FORMAT),
        'Page' => 1,
      ],
    ];
    $this->logger->notice(
      'Requesting Orange DAM content deleted since @time.', [
        '@time' => $formattedDeletedSince,
      ]
    );
    // Repeat the request until all items are retrieved.
    do {
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()
          ->objectManagement()
          ->listDeletedObjects($params);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if (empty($content)) {
        $this->logger->error('Invalid Orange DAM response data.');
        return $queued;
      }
      // Validate that items were found.
      if (empty($content->deletedObjects)) {
        if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
          $this->logger->debug('No deleted Orange DAM content found.');
        }
        return $queued;
      }
      // Configure the progress bar on the first iteration of the loop.
      if (!isset($nextPage)) {
        $progressBar->start();
      }
      // Queue the items for deletion.
      foreach ($content->deletedObjects as $object) {
        $item = new OrangeDamContent(
          $object->identifier,
          $object,
        );
        $this->migrationDataManager->queueDeletedItem($item);
        $queued++;
      }
      // Determine if we need to request the next page and increment the pager.
      $nextPage = isset($content->nextPageUrl);
      $params['query']['Page']++;
      // Update the progress bar.
      $progressBar->advance();
      if (!$nextPage) {
        $progressBar->finish();
        $progressBar->clear();
      }
    } while ($nextPage);
    return $queued;
  }

  /**
   * Retrieves Orange DAM keywords that have been updated since the passed time.
   *
   * The keywords are also queued.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeUpdatedSince
   *   The time to check for items changed since.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeUpdatedUntil
   *   The time to check for items changed until. Defaults to the current time.
   * @param string[] $tagTypes
   *   The array of tag types to filter the results by.
   *
   * @return array
   *   The number of Orange DAM keywords that have been queued.
   */
  public function getKeywords(
    ?DrupalDateTime $timeUpdatedSince,
    ?DrupalDateTime $timeUpdatedUntil,
    array $tagTypes = [],
  ): array {
    $keywords = [];
    $totalCount = 0;
    $progressBar = new ProgressBar(new ConsoleOutput());
    $keywordRelationships = $this->getKeywordRelationships();
    $allowedTagTypes = $tagTypes ?: $this->configFactory->get('orange_dam.settings')->get('tag_types');
    $itemsPerPage = $this->configFactory->get('orange_dam.settings')->get('api_items_per_page_keywords');
    // Validate the items per page value.
    if (empty($itemsPerPage)) {
      $this->logger->warning('The :config config value can not be "0" or empty.', [
        ':config' => 'api_items_per_page_keywords',
      ]);
    }
    // Build the request parameters.
    $params = [
      'format' => 'json',
      'CurrentPage' => 1,
      'ItemsPerPage' => $itemsPerPage,
      'CoreField.To-be-vetted' => 'false',
    ];
    // Add edit date filters.
    if ($timeUpdatedSince) {
      $formatted_date = $timeUpdatedSince->format(DataTable::DATE_FORMAT);
      $params['CoreField.EditDate>'] = $formatted_date;
      $this->logger->notice(
        'Requesting Orange DAM keywords updated since @time.',
        ['@time' => $formatted_date]
      );
    }
    if ($timeUpdatedUntil) {
      $params['CoreField.EditDate<'] = $timeUpdatedUntil->format(DataTable::DATE_FORMAT);
    }
    // Repeat the request until all items are retrieved.
    do {
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()->dataTable()->listKeywords($params);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if (empty($content)) {
        throw new \Exception(sprintf(
          'Unexpected Orange DAM API response. Received %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      // Validate that items were found and log an explanation for debugging.
      if (empty($content->Response)) {
        if ($timeUpdatedSince) {
          if ($tagTypes !== []) {
            if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
              $this->logger->debug(
                'Found no updated Orange DAM keywords of type :types updated since @updated_time.',
                [
                  ':types' => implode(', ', $tagTypes),
                  '@updated_time' => $timeUpdatedSince->setTimezone(new \DateTimeZone('utc'))->format('c'),
                ]
              );
            }
          }
          else {
            if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
              $this->logger->debug(
                'Found no Orange DAM keywords updated since @updated_time.',
                ['@updated_time' => $timeUpdatedSince->setTimezone(new \DateTimeZone('utc'))->format('c')]
              );
            }
          }
        }
        elseif ($tagTypes !== []) {
          $this->logger->warning(
            'No Orange DAM keywords of type :types found. Please check your keyword types and try again.', [
              ':types' => implode(', ', $tagTypes),
            ]);
        }
        return $keywords;
      }
      // Record the unusual behavior to set expectations.
      $this->logger->info(
        'Due to a limitation of the Orange DAM API, we are unable to filter by "Tag-type" in the request and must filter the returned data instead.'
      );
      // Configure the progress bar on the first iteration of the loop.
      if (!isset($nextPage)) {
        $totalCount = $content->ResponseSummary->TotalItemCount;
        $steps = (int) ceil($totalCount / $itemsPerPage);
        $progressBar->setMaxSteps($steps);
        $progressBar->start();
      }
      // Build an array of keywords.
      foreach ($content->Response as $item) {
        // Only queue keywords from white-listed tag types.
        // Adding multiple values to the request parameters is not supported by
        // the DataTable 2.1 or 2.2 API. Thus, we are not able to filter by the
        // Tags.CoreField.KeyType in the request and must use this work-around.
        if (!in_array($item->{'CoreField.Tag-type'}, $allowedTagTypes)) {
          continue;
        }
        // Add the parent keyword ID if a value is found.
        $item->parentKeywordRecordID = $keywordRelationships[$item->RecordID] ?? '';
        // Build each keyword item.
        $keywords[] = new OrangeDamKeyword(
          $item->RecordID,
          $item,
          $item->{'CoreField.Tag-type'},
        );
      }
      // Determine if we need to request the next page and increment the pager.
      $nextPage = isset($content->ResponseSummary->NextPage);
      $params['CurrentPage']++;
      // Update the progress bar.
      $progressBar->advance();
      if (!$nextPage) {
        $progressBar->finish();
        $progressBar->clear();
      }
    } while ($nextPage);
    // Display a final summary.
    if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
      $this->logger->debug(
        'Orange DAM allowed keywords filter removed :count keywords.',
        [':count' => $totalCount - count($keywords)]
      );
    }
    return $keywords;
  }

  /**
   * Get subtitles for an item.
   *
   * @param string $recordId
   *   The item's Orange Logic Record ID.
   *
   * @return object|false
   *   The Orange Logic subtitle data object, or false on error.
   *
   *   The "content" property in the returned value is the primary content.
   */
  public function getSubtitles(string $recordId) {
    $attempts = 0;
    // Get the maximum attempts configuration.
    $maxAttempts = $this->configFactory->get('orange_dam.settings')->get('api_max_retry_attempts');
    if (empty($maxAttempts)) {
      throw new \Exception('Missing Orange DAM Maximum API Retry Attempts ("orange_dam.settings api_max_retry_attempts") configuration.');
    }
    do {
      $attempts++;
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()->mediaFile()->getCaptions($recordId);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (ConnectException $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        $this->logger->warning(
          'An Orange DAM API connection exception occurred when requesting the media file captions for item ":id". Retrying the API request.', [
            ':id' => $recordId,
          ]);
        // Retry the request.
        continue;
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if ($response->getStatusCode() != static::HTTP_STATUS_OK) {
        throw new \Exception(sprintf(
          'Unexpected Orange DAM API response. Received %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      if (empty($content) || !isset($content[0]->content)) {
        $this->logger->debug('Empty caption data for media file with record ID :item.', [
          ':item' => $recordId,
        ]);
        return FALSE;
      }
      return $content[0];
    } while ($attempts <= $maxAttempts);
    // If it still failed after multiple attempts, log an error.
    $this->logger->error(
      'Giving up on retrieving the subtitles for item :item after :attempts attempts. Orange DAM API timed out after :timeout seconds.', [
        ':attempts' => $maxAttempts,
        ':item' => $recordId,
        ':timeout' => $this->getRequestTimeout(),
      ]);
    return FALSE;
  }

  /**
   * Get and queue the deleted keywords.
   *
   * @return int|false
   *   The number of items queued for deletion or FALSE if something failed.
   */
  public function getDeletedKeywords(
    DrupalDateTime $timeDeletedSince,
    DrupalDateTime $timeDeletedUntil = new DrupalDateTime(),
  ) {
    $queued = 0;
    $totalCount = 0;
    $progressBar = new ProgressBar(new ConsoleOutput());
    $syncedTagTypes = $this->configFactory->get('orange_dam.settings')->get('tag_types');
    $itemsPerPage = $this->configFactory->get('orange_dam.settings')->get('api_items_per_page_keywords');
    // Validate the items per page value.
    if (empty($itemsPerPage)) {
      $this->logger->warning('The :config config value can not be "0" or empty.', [
        ':config' => 'api_items_per_page_keywords',
      ]);
    }
    $formattedDeletedSince = $timeDeletedSince->format(ObjectManagement::DATE_FORMAT);
    // Build the request parameters.
    $params = [
      'format' => 'json',
      'CurrentPage' => 1,
      'ItemsPerPage' => $itemsPerPage,
      'Tags.CoreField.EditDate>' => $formattedDeletedSince,
      'Tags.CoreField.EditDate<' => $timeDeletedUntil->format(ObjectManagement::DATE_FORMAT),
      'Tags.CoreField.Disabled' => 1,
    ];
    $this->logger->notice(
      'Requesting Orange DAM keywords deleted since @time.',
      ['@time' => $formattedDeletedSince]
    );

    // Repeat the request until all items are retrieved.
    do {
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()->dataTable()->listKeywordDeletions($params);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if (empty($content)) {
        throw new \Exception(sprintf(
          'Unexpected Orange DAM API response. Received %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      // Build a progress bar.
      if (!isset($nextPage)) {
        $totalCount = $content->ResponseSummary->TotalItemCount;
        $steps = (int) ceil($totalCount / $itemsPerPage);
        if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
          $this->logger->debug('Found :count deleted Orange DAM keywords(s).', [
            ':count' => number_format($totalCount),
          ]);
        }
        $progressBar->setMaxSteps($steps);
        $progressBar->start();
      }
      // Build an array of deleted keywords.
      foreach ($content->Response as $item) {
        // Only queue keywords from white-listed tag types.
        // Adding multiple values to the request parameters is not supported by
        // the DataTable 2.1 or 2.2 API. Thus, we are not able to filter by the
        // Tags.CoreField.KeyType in the request and must use this work-around.
        $type = $item->{'Tags.CoreField.KeyType'};
        if (!in_array($type, $syncedTagTypes)) {
          continue;
        }
        $keyword = new OrangeDamKeyword(
          $item->RecordID,
          $item,
          $type,
        );
        $this->migrationDataManager->queueDeletedItem($keyword);
        $queued++;
      }
      // Determine if we need to request the next page and increment the pager.
      $nextPage = isset($content->ResponseSummary->NextPage);
      $params['CurrentPage']++;
      // Update the progress bar.
      $progressBar->advance();
      if (!$nextPage) {
        $progressBar->finish();
        $progressBar->clear();
      }
    } while ($nextPage);
    // Display a final summary.
    if ($this->configFactory->get('system.logging')->get('error_level') === 'verbose') {
      $this->logger->debug(
        'Orange DAM allowed keywords filter removed :count keywords.',
        [':count' => $totalCount - $queued]
      );
    }
    return $queued;
  }

  /**
   * Get the keyword relationship data from Orange DAM.
   *
   * @return string[]
   *   An array of parent keyword ID data keyed by keyword ID.
   */
  protected function getKeywordRelationships() {
    $keywordRelationships = [];
    // Build the request parameters.
    $params = [
      'format' => 'json',
      'CurrentPage' => 1,
      'ItemsPerPage' => $this->configFactory->get('orange_dam.settings')->get('api_items_per_page_keywords'),
    ];
    // Repeat the request until all items are retrieved.
    do {
      try {
        /** @var \Chromatic\OrangeDam\Http\Response $response */
        $response = $this->orangeDamConnection()->dataTable()->listKeywordRelationships($params);
        $this->eventDispatcher->dispatch(
          new OrangeDamApiResponseReceivedEvent($response),
          OrangeDamApiResponseEvents::RECEIVED
        );
        $content = $response->getData();
      }
      catch (\Exception $e) {
        $variables = Error::decodeException($e);
        $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
        throw new \Exception($e->getMessage(), $e->getCode(), $e);
      }
      // Validate the response.
      if (empty($content)) {
        throw new \Exception(sprintf(
          'Unexpected Orange DAM API response. Received %s with message "%s."',
          $response->getStatusCode(),
          $response->getReasonPhrase(),
        ));
      }
      // Build an array of keyword relationship data..
      foreach ($content->Response as $item) {
        $keywordRelationships[$item->RecordID] = $item->{'CoreField.KeyidFather'};
      }
      // Determine if we need to request the next page and increment the pager.
      $nextPage = isset($content->ResponseSummary->NextPage);
      $params['CurrentPage']++;
    } while ($nextPage);
    return $keywordRelationships;
  }

  /**
   * Get a comma separated string of field names to retrieve from Search API.
   *
   * @return string
   *   A comma separated string of field names.
   */
  protected function getSearchFieldNamesCommaSeparated(): string {
    return implode(
      ',',
      $this->orangeDamConfigurationManager->getSearchApiFields()
    );
  }

  /**
   * Check if the Orange DAM API is configured.
   *
   * When orangeDam is configured, the refresh token will be set.
   *
   * @return bool
   *   True if the OAuth token is set. False, otherwise.
   */
  protected function isConfigured(): bool {
    return !empty($this->orangeDamConnection->getClient()->token);
  }

  /**
   * Initialize and return the Orange DAM client instance.
   *
   * @return \Chromatic\OrangeDam\Factory
   *   Orange DAM API client instance.
   */
  protected function orangeDamConnection(): OrangeDamConnection {
    if (!$this->orangeDamConnection instanceof OrangeDamConnection) {
      // Validate that the needed configuration is available.
      $configuration = $this->configFactory->get('orange_dam.settings')->get();
      // Apply any overrides set in configOverrides().
      if (!empty($this->configOverrides)) {
        $configuration = [...$configuration, ...$this->configOverrides];
      }
      if (!array_key_exists('base_path', $configuration) || empty($configuration['base_path'])) {
        throw new \Exception('Missing Orange DAM Base Path ("orange_dam.settings base_path") configuration.');
      }
      $basePath = $configuration['base_path'];
      // Default a missing or NULL query_string to ''.
      $queryString = $configuration['query_string'] ?? '';
      // Get an Orange Dam connection handle. (The connection is also an
      // endpoint factory).
      $this->orangeDamConnection = new OrangeDamConnection(
        [
          'base_path' => $basePath,
          'query_string' => $queryString,
        ],
        NULL,
        [
          'http_errors' => FALSE,
          'timeout' => $this->getRequestTimeout(),
        ]
      );
    }
    // Update the OAuth token if it is expired.
    if (!$this->isConfigured() || $this->state->get(static::OAUTH_EXPIRE_IN) < $this->time->getRequestTime()) {
      // Validate that the needed configuration is available.
      if (empty($configuration['client_id'])) {
        throw new \Exception('Missing Orange DAM Client ID ("orange_dam.settings client_id") configuration.');
      }
      if (empty($configuration['client_secret'])) {
        throw new \Exception('Missing Orange DAM Client Secret ("orange_dam.settings client_secret") configuration.');
      }
      // Generate the tokens.
      $tokens = $this->orangeDamConnection->oAuth2()->getTokensByCode(
        $configuration['client_id'],
        $configuration['client_secret']
      );
      // Adds the OAuth token to the request.
      $this->orangeDamConnection->getClient()->setOauth2Token($tokens->access_token);
      // Store the expires in time.
      $this->state->set(
        static::OAUTH_EXPIRE_IN,
        ($tokens->expires_in + $this->currentRequest->server->get('REQUEST_TIME'))
      );
    }
    return $this->orangeDamConnection;
  }

  /**
   * Build an Orange DAM Content object from the Search API response.
   *
   * @param object $apiItem
   *   The content item as returned from the Search API.
   *
   * @return \Drupal\orange_dam\OrangeDamContent
   *   The representation of an Orange DAM object. Implements
   *   OrangeDamItemInterface.
   */
  protected function buildOrangeDamContentItem(object $apiItem): OrangeDamContent {
    $systemIdentifier = $apiItem->SystemIdentifier;
    $contentType = $apiItem->{OrangeDamContent::CONTENT_TYPE_PROPERTY};
    $data = $apiItem;
    if ($this->includeDataTableData($contentType)) {
      $data = (object) array_merge(
        (array) $apiItem,
        (array) $this->getDataTableData($systemIdentifier, DataTableIdentifiers::SYSTEM_ID),
      );
    }
    return new OrangeDamContent(
      $systemIdentifier,
      $data,
      $contentType,
    );
  }

  /**
   * Get the default API request timeout.
   *
   * @return int
   *   The request timeout in seconds.
   */
  protected function getRequestTimeout(): int {
    return (int) $this->configFactory->get('orange_dam.settings')->get('api_request_timeout') ?: static::DEFAULT_API_REQUEST_TIMEOUT;
  }

  /**
   * Determine if DataTable data should be added to the content item.
   *
   * @param string $contentType
   *   The content type.
   */
  protected function includeDataTableData(string $contentType): bool {
    if (!array_key_exists($contentType, $this->dataTableContentTypes)) {
      $contentTypes = $this->orangeDamConfigurationManager->getContentTypes();
      $this->dataTableContentTypes[$contentType] = array_key_exists($contentType, $contentTypes)
        && array_key_exists('use_datatable', $contentTypes[$contentType])
        && $contentTypes[$contentType]['use_datatable'];
    }
    return $this->dataTableContentTypes[$contentType];
  }

}
