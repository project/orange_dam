<?php

namespace Drupal\orange_dam;

/**
 * The representation of an Orange DAM file stream.
 */
interface OrangeDamFileStreamInterface {

  /**
   * Calculates and returns the correct extension for the file.
   */
  public function getExtension(): string;

  /**
   * Gets the file stream's mimetype.
   */
  public function getMimetype(): string;

  /**
   * Gets the file stream's size.
   */
  public function getSize(): int;

  /**
   * Gets the file stream.
   */
  public function getStream();

}
