<?php

namespace Drupal\orange_dam;

/**
 * The representation of an Orange DAM object.
 */
class OrangeDamKeyword implements OrangeDamItemInterface {

  /**
   * The item type.
   */
  public const ITEM_TYPE = 'orange_dam_keyword';

  /**
   * The RecordID value.
   */
  public string $id;

  /**
   * The keyword data.
   */
  public object $data;

  /**
   * The item type.
   */
  public string $type;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $id, object $data, string $type) {
    $this->id = $id;
    $this->data = $data;
    $this->type = $type;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function data(): object {
    $data = $this->data;
    $data->{static::ITEM_TYPE_PROPERTY} = static::ITEM_TYPE;
    $data->{static::CONTENT_TYPE_PROPERTY} = $this->type();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function type(): string {
    return $this->type;
  }

}
