<?php

namespace Drupal\orange_dam;

use Drupal\Core\File\Exception\FileException;
use Psr\Http\Message\StreamInterface;

/**
 * The representation of an Orange DAM file stream.
 */
class OrangeDamFileStream implements OrangeDamFileStreamInterface {

  /**
   * The extension of the file.
   */
  protected string $extension;

  public function __construct(
    protected string $mimetype,
    protected int $size,
    protected StreamInterface $stream,
  ) {}

  /**
   * Calculates and returns the correct extension by mimetype.
   *
   * @todo Replace this with MimeTypeGuesser service.
   */
  public function getExtension(): string {
    if (!isset($this->extension)) {
      switch ($this->mimetype) {
        case 'application/epub+zip':
          $this->extension = 'epub';
          break;

        case 'application/msword':
          $this->extension = 'doc';
          break;

        case 'application/pdf':
          $this->extension = 'pdf';
          break;

        case 'application/rtf':
          $this->extension = 'rtf';
          break;

        case 'application/xml':
        case 'text/xml':
          $this->extension = 'xml';
          break;

        case 'application/zip':
          $this->extension = 'zip';
          break;

        case 'audio/3gpp':
        case 'video/3gpp':
          $this->extension = '3gp';
          break;

        case 'audio/aac':
          $this->extension = 'aac';
          break;

        case 'audio/mpeg':
          $this->extension = 'mp3';
          break;

        case 'audio/opus':
          $this->extension = 'opus';
          break;

        case 'audio/wav':
          $this->extension = 'wav';
          break;

        case 'audio/webm':
          $this->extension = 'weba';
          break;

        case 'image/apng':
          $this->extension = 'apng';
          break;

        case 'image/bmp':
          $this->extension = 'bmp';
          break;

        case 'image/gif':
          $this->extension = 'gif';
          break;

        case 'image/jpeg':
          $this->extension = 'jpg';
          break;

        case 'image/png':
          $this->extension = 'png';
          break;

        case 'image/svg+xml':
          $this->extension = 'svg';
          break;

        case 'image/tiff':
          $this->extension = 'tiff';
          break;

        case 'image/vnd.microsoft.icon':
          $this->extension = 'ico';
          break;

        case 'image/webp':
          $this->extension = 'webp';
          break;

        case 'text/csv':
          $this->extension = 'csv';
          break;

        case 'text/plain':
          $this->extension = 'txt';
          break;

        case 'video/mp2t':
          $this->extension = 'ts';
          break;

        case 'video/mp4':
          $this->extension = 'mp4';
          break;

        case 'video/mpeg':
          $this->extension = 'mpeg';
          break;

        case 'video/webm':
          $this->extension = 'webm';
          break;

        case 'video/x-msvideo':
          $this->extension = 'avi';
          break;

        default:
          $this->extension = '';
          throw new FileException('File extension derivation for mimetype ' . $this->mimetype . ' has not been implemented.');
      }
    }
    return $this->extension;
  }

  /**
   * {@inheritdoc}
   */
  public function getMimetype(): string {
    return $this->mimetype;
  }

  /**
   * {@inheritdoc}
   */
  public function getSize(): int {
    return $this->size;
  }

  /**
   * {@inheritdoc}
   */
  public function getStream(): StreamInterface {
    return $this->stream;
  }

}
