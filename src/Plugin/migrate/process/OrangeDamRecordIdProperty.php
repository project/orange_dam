<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Chromatic\OrangeDam\DataTableIdentifiers;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Get a property of an item from the DataTable API using a Record ID.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_record_id_property"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_record_id_property
 *   source: The item's Record ID.
 *   property: The property to return.
 * @endcode
 */
class OrangeDamRecordIdProperty extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    $data = $this->orangeDamApi->getDataTableData(
      $value,
      DataTableIdentifiers::RECORD_ID,
    );
    return $data?->{$this->configuration['property']};
  }

}
