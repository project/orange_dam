<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Chromatic\OrangeDam\DataTableIdentifiers;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Get multiple properties of an item from the DataTable API using a Record ID.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_record_id_properties"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_record_id_properties
 *   source: The item's Record ID.
 *   properties:
 *     - A property to return.
 *     - A property to return.
 * @endcode
 */
class OrangeDamRecordIdProperties extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    $data = $this->orangeDamApi->getDataTableData(
      $value,
      DataTableIdentifiers::RECORD_ID,
    );
    if ($data && array_key_exists('properties', $this->configuration)) {
      $requestedProperties = (array) $this->configuration['properties'];
      $returnedProperties = [];
      foreach ($requestedProperties as $property) {
        if (property_exists($data, $property)) {
          $returnedProperties[$property] = $data->{$property};
        }
      }

      return $returnedProperties;
    }

    // Return all properties if no specific ones were requested.
    return (array) $data;
  }

}
