<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\orange_dam\OrangeDamApi;
use Drupal\orange_dam\OrangeDamConfigurationManager;
use Drupal\orange_dam\OrangeDamQueueDataManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for migration plugins.
 */
abstract class OrangeDamProcessBase extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructor.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    protected OrangeDamQueueDataManager $orangeDamQueueDataManager,
    protected OrangeDamApi $orangeDamApi,
    protected OrangeDamConfigurationManager $orangeDamConfigurationManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->logger = $loggerChannelFactory->get('orange_dam.migration');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('logger.factory'),
      $container->get('orange_dam.queue_data_manager'),
      $container->get('orange_dam.api'),
      $container->get('orange_dam.configuration_manager'),
    );
  }

}
