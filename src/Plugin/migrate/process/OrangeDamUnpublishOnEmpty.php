<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\node\NodeInterface;

/**
 * Alter the published state if the passed in field is empty.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_unpublish_on_empty"
 * )
 *
 * @code
 * status:
 *   plugin: orange_dam_unpublish_on_empty
 *   source: foo
 *   field: bar
 *   field_key: my_key (Optional. Use if you need to target an array key or
 *   object property within the field value instead of the field value.)
 * @endcode
 */
class OrangeDamUnpublishOnEmpty extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty): int {
    // Validate that we have the needed configuration.
    if (empty($this->configuration['field'])) {
      throw new MigrateException('Missing "field" configuration in the "orange_dam_unpublish_on_empty" process plugin.');
    }
    // If the field is empty, mark the item as unpublished.
    $field_value = $row->get($this->configuration['field']);
    if (array_key_exists('field_key', $this->configuration) && $this->configuration['field_key']) {
      if (is_object($field_value) && property_exists($field_value, $this->configuration['field_key'])) {
        $field_value = $field_value->{$this->configuration['field_key']};
      }
      elseif (is_array($field_value) && array_key_exists($this->configuration['field_key'], $field_value)) {
        $field_value = $field_value[$this->configuration['field_key']];
      }
    }
    // During a migration, only empty strings are truly "empty".
    if (is_string($field_value) && trim($field_value) === '') {
      return NodeInterface::NOT_PUBLISHED;
    }
    // Leave the value unaltered.
    return $value;
  }

}
