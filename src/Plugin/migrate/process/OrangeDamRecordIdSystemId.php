<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Chromatic\OrangeDam\DataTableIdentifiers;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Get an item's System ID from its Record ID.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_record_id_system_id"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_record_id_system_id
 *   source: The item's Record ID.
 * @endcode
 */
class OrangeDamRecordIdSystemId extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    $data = $this->orangeDamApi->getDataTableData(
      $value,
      DataTableIdentifiers::RECORD_ID,
    );
    return $data?->{'SystemID'};
  }

}
