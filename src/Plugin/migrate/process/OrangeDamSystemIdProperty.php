<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Chromatic\OrangeDam\DataTableIdentifiers;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Get a property of an item from the DataTable API using a System ID.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_system_id_property"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_system_id_property
 *   source: The item's System Identifier
 *   property: The property to return.
 * @endcode
 */
class OrangeDamSystemIdProperty extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    $data = $this->orangeDamApi->getDataTableData(
      $value,
      DataTableIdentifiers::SYSTEM_ID,
    );
    return $data?->{$this->configuration['property']};
  }

}
