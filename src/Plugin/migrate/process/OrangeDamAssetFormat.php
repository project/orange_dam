<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Find the asset format associated with the item.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_asset_format"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_asset_format
 *   source: The content type of the item.
 * @endcode
 */
class OrangeDamAssetFormat extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    return $this->orangeDamConfigurationManager->getAssetFormat($value) ?: '';
  }

}
