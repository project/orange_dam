<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Find the asset URL associated with the item.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_asset_url"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_asset_url
 *   source:
 *     - The System ID of the item.
 *     - The content type of the item.
 * @endcode
 */
class OrangeDamAssetUrl extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    [$systemId, $contentType] = $value;
    return $this->orangeDamQueueDataManager->getAssetUrl($systemId, $contentType) ?: '';
  }

}
