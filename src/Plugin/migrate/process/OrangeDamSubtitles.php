<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Get the subtitles associated with an item using the item's Record ID.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_subtitles"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_subtitles
 *   source: The Record ID of the item.
 * @endcode
 */
class OrangeDamSubtitles extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    if ($subtitles = $this->orangeDamApi->getSubtitles($value)) {
      return $subtitles->content;
    }
    return '';
  }

}
