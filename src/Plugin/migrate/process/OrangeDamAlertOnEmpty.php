<?php

namespace Drupal\orange_dam\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Row;

/**
 * Alert on empty values in source.
 *
 * @MigrateProcessPlugin(
 *   id = "orange_dam_alert_on_empty"
 * )
 *
 * @code
 * field_foo:
 *   plugin: orange_dam_alert_on_empty
 *   source: The source field to check
 *   fallback: The fallback value.
 *   throw: Set to true if an exception should be thrown. Default: false.
 * @endcode
 */
class OrangeDamAlertOnEmpty extends OrangeDamProcessBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    if (!empty($value)) {
      return $value;
    }
    // Build the error message and log the issue.
    if (array_key_exists('source', $this->configuration)) {
      $message = $this->t(
        'Source field ":field" with mapping to ":destination" is empty for item :mig::source_id.', [
          ':field' => $this->configuration['source'],
          ':destination' => $destinationProperty,
          ':mig' => $row->get('constants/bundle'),
          ":source_id" => implode(', ', $row->getSourceIdValues()),
        ]
      );
    }
    else {
      $message = $this->t(
        'Source field, with mapping to ":destination", is empty for item :mig::source_id.', [
          ':destination' => $destinationProperty,
          ':mig' => $row->get('constants/bundle'),
          ":source_id" => implode(', ', $row->getSourceIdValues()),
        ]
      );
    }
    $this->logger->warning($message);
    // Throw an error if it is configured to do so.
    if (!empty($this->configuration['throw'])) {
      throw new MigrateSkipRowException($message, TRUE);
    }
    // Validate that a fallback value was configured.
    if (empty($this->configuration['fallback'])) {
      $message = $this->t(
        'Missing fallback configuration for the :destination field mapping in :mig.', [
          ':destination' => $destinationProperty,
          ':mig' => $row->get('constants/bundle'),
        ]
      );
      $this->logger->error($message);
      throw new MigrateSkipRowException($message);
    }
    return $row->get($this->configuration['fallback']);
  }

}
