<?php

namespace Drupal\orange_dam\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Provides Orange DAM migration queues via the deriver class.
 *
 * @QueueWorker(
 *   id = "orange_dam_migration",
 *   admin_label = @Translation("Orange DAM Migration Queue"),
 *   category = @Translation("Orange DAM"),
 *   deriver = "Drupal\orange_dam\Plugin\Derivative\OrangeDamMigrationQueues"
 * )
 */
class OrangeDamMigrationQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // This is only here to implement the interface method. Items in this queue
    // are processed via migrations.
  }

}
