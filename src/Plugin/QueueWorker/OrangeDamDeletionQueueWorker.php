<?php

namespace Drupal\orange_dam\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\orange_dam\OrangeDamQueueDataManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles deletion of Orange DAM items.
 *
 * @QueueWorker(
 *   id = "orange_dam_deletion_queue",
 *   title = @Translation("Orange DAM - Deletion Queue"),
 *   category = @Translation("Orange DAM"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class OrangeDamDeletionQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructor.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected OrangeDamQueueDataManager $queueDataManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('orange_dam.queue_data_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $processed = $this->queueDataManager->processDeletedQueueItem($data);
    if ($processed === 0) {
      $this->queueDataManager->logger()->debug(
        'Deletion queue item with internal id :id did not result in deletion. Ignoring.',
        [':id' => $data->id()]
      );
    }
  }

}
