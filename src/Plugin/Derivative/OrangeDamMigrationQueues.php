<?php

namespace Drupal\orange_dam\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates queue worker plugins to register queues.
 *
 * If no queue worker is registered for a queue it doesn't show up when running
 * drush queue:list and likely has other side effects.
 *
 * @see \Drupal\orange_dam\Plugin\QueueWorker\OrangeDamMigrationQueueWorker
 */
class OrangeDamMigrationQueues extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Create the queue deriver plugin class.
   */
  public function __construct(
    protected MigrationPluginManagerInterface $migrationManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    return new static(
      $container->get('plugin.manager.migration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    // Create a queue for each migration with the "orange_dam" tag so those
    // migrations can use these queues as their source.
    foreach ($this->migrationManager->createInstancesByTag('orange_dam') as $migration) {
      $this->derivatives[$migration->id()] = $basePluginDefinition;
      $this->derivatives[$migration->id()]['title'] = $migration->label();
    }
    return $this->derivatives;
  }

}
