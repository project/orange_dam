<?php

namespace Drupal\orange_dam;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Manages the retrieval and validation of configuration.
 */
class OrangeDamConfigurationManager {

  /**
   * The Orange DAM configuration.
   */
  const ORANGE_DAM_CONFIG = 'orange_dam.settings';

  /**
   * A map of content types to their respective asset formats.
   *
   * This is based on the content_types list in orange_dam.settings config.
   */
  protected array $contentTypeAssetFormats;

  /**
   * An array of valid watermarked asset formats.
   *
   * This is derived from the content_types list in orange_dam.settings config.
   */
  protected array $watermarkedAssetFormats;

  /**
   * Constructor.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * Subtract the api_request_time_offset from the passed datetime.
   */
  public function applyApiTimeOffset(\DateTime|DrupalDateTime &$datetime): void {
    // Based on ISO 8601 duration specification.
    // @see https://www.php.net/manual/en/dateinterval.construct.php
    $offset_duration = 'PT' . (int) $this->configFactory->get(static::ORANGE_DAM_CONFIG)
      ->get('api_request_time_offset') . 'S';
    $offsetInterval = new \DateInterval($offset_duration);
    $datetime->sub($offsetInterval);
  }

  /**
   * Get an array of allowed content type names.
   *
   * @return string[]
   *   An array of allowed content type names.
   */
  public function getContentTypeNames(): array {
    return array_keys($this->getContentTypes());
  }

  /**
   * Returns whether the dispatch_responses setting is enabled.
   */
  public function dispatchResponsesEnabled(): bool {
    return (bool) $this->configFactory
      ->get(static::ORANGE_DAM_CONFIG)
      ->get('dispatch_responses');
  }

  /**
   * Get the content type configuration.
   *
   * @return array
   *   An array of content type configuration.
   */
  public function getContentTypes(): array {
    $contentTypes = $this->configFactory
      ->get(static::ORANGE_DAM_CONFIG)
      ->get('content_types');
    if (empty($contentTypes)) {
      throw new \Exception(
        'Missing "orange_dam.settings content_types" configuration.'
      );
    }
    return $contentTypes;
  }

  /**
   * Get an array of allowed keyword types.
   *
   * @return string[]
   *   An array of allowed content types.
   */
  public function getKeywordTypes(): array {
    $tagTypes = $this->configFactory
      ->get(static::ORANGE_DAM_CONFIG)
      ->get('tag_types');
    if (empty($tagTypes)) {
      throw new \Exception(
        'Missing "orange_dam.settings tag_types" configuration.'
      );
    }
    return $tagTypes;

  }

  /**
   * Get an array of fields to request from the Orange DAM Search API.
   *
   * @return string[]
   *   An array of field names.
   */
  public function getSearchApiFields(): array {
    $searchApiFields = $this->configFactory
      ->get(static::ORANGE_DAM_CONFIG)
      ->get('search_api_fields');
    if (empty($searchApiFields)) {
      throw new \Exception(
        'Missing "orange_dam.settings search_api_fields" configuration.'
      );
    }
    return $searchApiFields;

  }

  /**
   * Get a list of asset formats keyed by their associated content type.
   *
   * @return string[]
   *   A list of asset formats keyed by their associated content type.
   */
  public function getAssetFormats(): array {
    if (!isset($this->contentTypeAssetFormats)) {
      $contentTypes = $this->getContentTypes();
      $this->contentTypeAssetFormats = [];
      foreach ($contentTypes as $contentType => $contentTypeConfiguration) {
        if (isset($contentTypeConfiguration['asset_format'])) {
          $this->contentTypeAssetFormats[$contentType] = $contentTypeConfiguration['asset_format'];
        }
      }
    }
    return $this->contentTypeAssetFormats;
  }

  /**
   * Determine the asset format to use for a given content type.
   *
   * @param string $contentType
   *   The content type.
   * @param bool $watermarked
   *   Optional directive for whether to retrieve the watermarked asset format.
   *
   * @return string
   *   The content type's default asset format or watermarked asset format.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   */
  public function getAssetFormat(string $contentType, bool $watermarked = FALSE): string {
    if ($watermarked) {
      $watermarked_asset_format = $this->getContentTypes()[$contentType]['asset_format_watermarked'] ?? NULL;
      if (is_null($watermarked_asset_format)) {
        throw new ConfigValueException(sprintf('No watermarked asset format configured for content type %s', $contentType));
      }
      else {
        return $watermarked_asset_format;
      }
    }
    // @todo Consider making assetFormats a class variable for re-use.
    $assetFormats = $this->getAssetFormats();
    if (isset($assetFormats[$contentType])) {
      return $assetFormats[$contentType];
    }
    throw new ConfigValueException(sprintf('No asset format configured for content type %s', $contentType));
  }

  /**
   * Evaluate whether an asset format is a configured watermarked asset format.
   *
   * @return bool
   *   Whether the passed format is a watermarked asset format or not.
   */
  public function isWatermarkedAssetFormat(string $format): bool {
    if (!isset($this->watermarkedAssetFormats)) {
      $contentTypes = $this->getContentTypes();
      $this->watermarkedAssetFormats = [];
      foreach ($contentTypes as $contentTypeConfiguration) {
        if (
          isset($contentTypeConfiguration['asset_format_watermarked'])
          && !array_key_exists($contentTypeConfiguration['asset_format_watermarked'], $this->watermarkedAssetFormats)
        ) {
          $this->watermarkedAssetFormats[$contentTypeConfiguration['asset_format_watermarked']] = $contentTypeConfiguration['asset_format_watermarked'];
        }
      }
    }
    return isset($this->watermarkedAssetFormats[$format]);
  }

}
