<?php

namespace Drupal\orange_dam;

/**
 * Manages preparing content for migrations and building the migration queues.
 */
interface OrangeDamMigrationDataManagerInterface {

  /**
   * Runs Orange DAM related migrations.
   *
   * @param array $migrationIds
   *   An array of migration ID values to run. Defaults to all migrations.
   * @param array $options
   *   An array of options to pass to the migration. Suggested options are:
   *   - limit: The number of items to limit each migration run to.
   *   - idlist: An array of source ID values to limit the migration to.
   * @param bool $disableDuplicateCheck
   *   Set if duplicates checking should be disabled.
   */
  public function runMigrations(
    array $migrationIds = [],
    array $options = [],
    bool $disableDuplicateCheck = TRUE,
  );

  /**
   * Queue an item for further processing in a migration.
   *
   * @param \Drupal\orange_dam\OrangeDamItemInterface $item
   *   The Orange DAM item.
   */
  public function queueMigrationItem(OrangeDamItemInterface $item): void;

  /**
   * Get the list of Orange DAM migrations.
   *
   * @param string $migrationTag
   *   A tag associated with a migration.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[]
   *   The Orange DAM migrations.
   */
  public function getMigrations(string $migrationTag): array;

}
