<?php

namespace Drupal\orange_dam;

/**
 * The representation of an Orange DAM item.
 */
interface OrangeDamItemInterface {

  /**
   * Data property that stores the item type (e.g. content, keyword).
   */
  public const ITEM_TYPE_PROPERTY = 'orange_dam_item_type';

  /**
   * Data property that stores the content type (e.g. photo, video).
   */
  public const CONTENT_TYPE_PROPERTY = 'orange_dam_content_type';

  /**
   * Build a representation of an Orange DAM item.
   *
   * @param string $id
   *   The item ID.
   * @param object $data
   *   The raw data from an Orange DAM item.
   * @param string $type
   *   The type of item.
   */
  public function __construct(string $id, object $data, string $type);

  /**
   * Gets the item ID.
   *
   * @return string
   *   The item ID.
   */
  public function id(): string;

  /**
   * Gets the item data as an object.
   *
   * @return object
   *   The item data.
   */
  public function data(): object;

  /**
   * Get the item type.
   *
   * @return string
   *   The item type.
   */
  public function type(): string;

}
