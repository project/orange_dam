<?php

namespace Drupal\orange_dam;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Manages retrieving a list of updated content and building a queue.
 */
interface OrangeDamQueueDataManagerInterface {

  /**
   * Update the list of keywords that Orange DAM has deleted.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeDeletedSince
   *   The time to check for deletions since. Defaults to the last time ran.
   *
   * @return int|false
   *   The number of items queued for syncing or false on failure.
   */
  public function queueKeywordDeletions(?DrupalDateTime $timeDeletedSince = NULL): int|false;

  /**
   * Update the list of items that Orange DAM has new information for.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeUpdatedSince
   *   The time to check for updates since. Defaults to the last time ran.
   *
   * @return int|false
   *   The number of items queued for syncing or false on failure.
   */
  public function queueUpdatedItems(?DrupalDateTime $timeUpdatedSince = NULL): int|false;

  /**
   * Update single item from Orange DAM.
   *
   * @param string $systemIdentifier
   *   The unique ID of the item in Orange DAM. Example: EX1STO431061.
   *
   * @return \Drupal\orange_dam\OrangeDamItemInterface|false
   *   The item or false if the operation failed.
   */
  public function updateSingleItem(string $systemIdentifier): OrangeDamItemInterface|false;

  /**
   * Update the list of keywords that Orange DAM has new information for.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeUpdatedSince
   *   The time to check for updates since. Defaults to the last time ran.
   *
   * @return int
   *   The number of keywords queued for syncing.
   */
  public function queueUpdatedKeywords(?DrupalDateTime $timeUpdatedSince = NULL): int;

  /**
   * Finds and deletes all items deleted since a given time.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $timeDeletedSince
   *   The time to check for deleted items since. Defaults to the last time ran.
   *
   * @return int|false
   *   The number of items queued for deletion or false on failure.
   */
  public function queueContentDeletions(?DrupalDateTime $timeDeletedSince = NULL): int|false;

}
