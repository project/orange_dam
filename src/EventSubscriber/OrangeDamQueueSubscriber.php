<?php

namespace Drupal\orange_dam\EventSubscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\orange_dam\Event\OrangeDamDeletionQueueEvents;
use Drupal\orange_dam\Event\OrangeDamDeletionQueuePreDeleteEvent;
use Drupal\orange_dam\OrangeDamKeyword;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Checks for duplicate items to handle content type changes.
 */
class OrangeDamQueueSubscriber implements EventSubscriberInterface {

  /**
   * Constructor.
   */
  public function __construct(
    protected Connection $db,
    protected LoggerChannelInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      OrangeDamDeletionQueueEvents::PRE_DELETE => ['itemPreDeleteSynchronize'],
    ];
  }

  /**
   * Handle a deletion queue item before it is deleted.
   */
  public function itemPreDeleteSynchronize(OrangeDamDeletionQueuePreDeleteEvent $event): void {
    /** @var \Drupal\orange_dam\OrangeDamItemInterface $item */
    $item = $event->getItem();
    // @todo Implement queue synchronization for keywords.
    // @see https://www.drupal.org/project/orange_dam/issues/3443602
    if ($item instanceof OrangeDamKeyword) {
      return;
    }
    // @todo Querying against serialized data blobs is less-than-ideal, but
    //   de-serializing thousands of rows and interrogating each object for the
    //   desired key is worse.
    // @todo Come up with a better way to establish this query, since it cannot
    //   be guaranteed to work if the Queue module changes its schema.
    $migrationItems = $this->db->select('queue', 'q')
      ->fields('q', ['item_id', 'name', 'created'])
      ->condition('q.name', 'orange_dam_migration:%', 'LIKE')
      ->condition('q.data', '%"SystemIdentifier";s:' . mb_strlen($item->id()) . ':"' . $this->db->escapeLike($item->id()) . '"%', 'LIKE')
      ->execute()
      ->fetchAll();

    // The Orange Logic timestamp format is not parseable by DateTime or
    // DrupalDateTime, so we have to use strtotime().
    $itemDeletionTimestamp = strtotime($item->data()->deletionDate);
    if ($migrationItems) {
      // Check all matching items in the migration queue to see if there is a
      // more recent deletion instruction in the deletion queue.
      foreach ($migrationItems as $migrationItem) {
        if ($itemDeletionTimestamp >= $migrationItem->created) {
          // Delete migration item and continue.
          $this->db->delete('queue')
            ->condition('item_id', $migrationItem->item_id)
            ->execute();
          $this->logger->notice(
            'Removing :name item :id from the migration queue since there is a more recent deletion queue item for it.',
            [
              ':name' => $migrationItem->name,
              ':id' => $migrationItem->item_id,
            ]
          );
        }
        else {
          $this->logger->notice(
            'Ignoring queued deletion item :id since a more recent corresponding migration item is queued.',
            [
              ':id' => $item->id(),
            ]
          );
          // Tell dispatcher to consider this item fully processed.
          $event->setHandled(TRUE);
        }
      }
    }
  }

}
