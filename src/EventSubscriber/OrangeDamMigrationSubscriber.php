<?php

namespace Drupal\orange_dam\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\orange_dam\Event\OrangeDamDuplicateItemDeletionEvent;
use Drupal\orange_dam\Event\OrangeDamDuplicateItemDetectionEvent;
use Drupal\orange_dam\Event\OrangeDamDuplicateItemEvents;
use Drupal\orange_dam\OrangeDamApi;
use Drupal\orange_dam\OrangeDamContent;
use Drupal\orange_dam\OrangeDamItemInterface;
use Drupal\orange_dam\OrangeDamKeyword;
use Drupal\orange_dam\OrangeDamMigrationDataManager;
use Drupal\pathauto\PathautoGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Checks for duplicate items to handle content type changes.
 */
class OrangeDamMigrationSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructor.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerChannelInterface $logger,
    protected OrangeDamApi $orangeDamApi,
    protected OrangeDamMigrationDataManager $migrationDataManager,
    protected ConfigFactoryInterface $configFactory,
    protected EventDispatcherInterface $eventDispatcher,
    protected PathautoGeneratorInterface $pathautoGenerator,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      MigrateEvents::POST_ROW_SAVE => ['checkExistingContent'],
    ];
  }

  /**
   * Handle the post row save checks.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The migration event.
   */
  public function checkExistingContent(MigratePostRowSaveEvent $event): void {
    // Determine if we should check for duplicates.
    if (!$this->isCheckForDuplicatesEnabled()) {
      return;
    }
    if (!$this->isCheckForDuplicatesEnabledForRow($event)) {
      return;
    }
    // Get information about the migrated entity.
    $sourceIds = $event->getRow()->getSourceIdValues();
    $destinationIds = $event->getDestinationIdValues();
    $migratedEntityId = array_pop($destinationIds);
    // @todo Address upstream the fact that getDerivativeId() is not
    //   guaranteed to exist on destination plugins.
    // @see https://www.drupal.org/project/drupal/issues/2783715
    $entityType = $event->getMigration()->getDestinationPlugin()->getDerivativeId();
    // Load the migrated entity.
    $migratedEntity = $this->entityTypeManager
      ->getStorage($entityType)
      ->load($migratedEntityId);
    // Build an Orange DAM item with the source data.
    $item = new OrangeDamContent(
      array_pop($sourceIds),
      (object) $event->getRow()->getSource(),
    );
    // Find any duplicate entities and delete them.
    $entitiesToDelete = $this->findDuplicates($item, $migratedEntity);
    foreach ($entitiesToDelete as $entityToDelete) {
      $this->deleteDuplicate($entityToDelete, $migratedEntity);
    }
    // Log information about the new entity only when duplicates were found.
    if ($entitiesToDelete !== []) {
      $this->logger->notice(
        ':system_id: Created a new ":type" ":title" because it changed its type in Orange DAM.',
        [
          ':system_id' => $migratedEntity->field_system_id->value,
          ':title' => $migratedEntity->label(),
          ':type' => $migratedEntity->bundle(),
        ]
      );
    }
    // Update the alias on the new entity now that the alias is removed from the
    // original entity.
    $this->pathautoGenerator->updateEntityAlias($migratedEntity, 'update');
  }

  /**
   * Find existing entities with the same system identifier.
   *
   * @param \Drupal\orange_dam\OrangeDamItemInterface $item
   *   The item to check.
   * @param \Drupal\Core\Entity\EntityInterface $migratedEntity
   *   The entity that was just migrated.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The duplicate entities found.
   */
  protected function findDuplicates(OrangeDamItemInterface $item, EntityInterface $migratedEntity): array {
    /** @var \Drupal\orange_dam\Event\OrangeDamDuplicateItemDetectionEvent $orangeDamDuplicateEvent */
    $orangeDamDuplicateEvent = $this->eventDispatcher->dispatch(
      new OrangeDamDuplicateItemDetectionEvent($item, $migratedEntity),
      OrangeDamDuplicateItemEvents::DETECTED,
    );
    return $orangeDamDuplicateEvent->getDuplicateEntities();
  }

  /**
   * Delete a duplicate entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entityDelete
   *   The entity to delete.
   * @param \Drupal\Core\Entity\EntityInterface $entityNew
   *   The new entity that is replacing the duplicate ones.
   */
  protected function deleteDuplicate(EntityInterface $entityDelete, EntityInterface $entityNew): void {
    // Allow other modules to respond to duplicate entity deletion.
    $this->eventDispatcher->dispatch(
      new OrangeDamDuplicateItemDeletionEvent($entityDelete, $entityNew),
      OrangeDamDuplicateItemEvents::DELETED,
    );
    // Delete the old entity.
    $this->logger->notice('Deleted :otype :oeid (replaced by :ntype :neid)',
      [
        ':otype' => $entityDelete->bundle(),
        ':oeid' => $entityDelete->id(),
        ':ntype' => $entityNew->bundle(),
        ':neid' => $entityNew->id(),
      ]
    );

    $entityDelete->delete();
  }

  /**
   * Determines if we should check for duplicate content items.
   *
   * @return bool
   *   Boolean indicating if we should check for duplicate items.
   */
  protected function isCheckForDuplicatesEnabled(): bool {
    static $checkForDuplicates;
    if ($checkForDuplicates === NULL) {
      // Check the Drupal configuration value.
      $checkForDuplicates = $this->configFactory
        ->get('orange_dam.settings')
        ->get('check_for_duplicates');
      // Allow for state overrides.
      $disableCheckForDuplicatesState = drupal_static(
        OrangeDamMigrationDataManager::DISABLE_DUPLICATE_CHECK_STATE_KEY,
      );
      if ($disableCheckForDuplicatesState === TRUE) {
        $checkForDuplicates = FALSE;
        $this->logger->warning('Duplicate checking disabled via CLI for the current migration process.');
      }
    }
    return $checkForDuplicates;
  }

  /**
   * Allow for duplicate checking to be disabled per various source types.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The migration event.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   */
  protected function isCheckForDuplicatesEnabledForRow(MigratePostRowSaveEvent $event): bool {
    $data = $event->getRow()->getSourceProperty('data');
    if (!is_array($data) || !array_key_exists(OrangeDamItemInterface::ITEM_TYPE_PROPERTY, $data)) {
      $message = $this->t(
        'Source data is missing required field :field. Cannot determine whether to check for duplicates.',
        [':field' => OrangeDamItemInterface::ITEM_TYPE_PROPERTY],
      );
      throw new MigrateSkipRowException($message);
    }
    // Keywords do not change their type in Orange DAM to our knowledge.
    return $data[OrangeDamItemInterface::ITEM_TYPE_PROPERTY] !== OrangeDamKeyword::ITEM_TYPE;
  }

}
