<?php

namespace Drupal\orange_dam;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_tools\MigrateExecutable;

/**
 * Manages preparing content for migrations and building the migration queues.
 */
class OrangeDamMigrationDataManager implements OrangeDamMigrationDataManagerInterface {

  use StringTranslationTrait;

  /**
   * The maximum number of items to process at one time in a migration.
   */
  public const MIGRATION_LIMIT = '100';

  /**
   * The tag which identifies a migration as an Orange Dam migrations.
   */
  public const MIGRATION_TAG = 'orange_dam';

  /**
   * The tag used to identify migrations related content.
   */
  public const MIGRATION_TAG_CONTENT = 'orange_dam_content';

  /**
   * The tag used to identify migrations related to taxonomy terms.
   */
  public const MIGRATION_TAG_TAXONOMY = 'orange_dam_taxonomy';

  /**
   * The log channel to use.
   *
   * @deprecated in orange_dam:2.2.0 and is removed from orange_dam:3.0.0. This
   *   constant has been superseded by logger.channel.orange_dam.migration
   *   service.
   * @see https://www.drupal.org/project/orange_dam/issues/3481057
   */
  public const LOGGER_CHANNEL = 'orange_dam.migration';

  /**
   * The deletion queue name.
   */
  public const DELETION_QUEUE = 'orange_dam_deletion_queue';

  /**
   * The number of seconds to keep the deletion queue item leased.
   */
  public const DELETION_QUEUE_LEASE_TIME = 5;

  /**
   * The duplicate check state key.
   */
  public const DISABLE_DUPLICATE_CHECK_STATE_KEY = 'orange_dam.duplicate_check';

  /**
   * Constructor.
   */
  public function __construct(
    protected LoggerChannelInterface $logger,
    protected QueueFactory $queueFactory,
    protected ConfigFactoryInterface $configFactory,
    protected MigrationPluginManagerInterface $migrationManager,
    protected QueueWorkerManager $queueManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function queueMigrationItem(OrangeDamItemInterface $item): void {
    // Some field names contain periods, which are not allowed in key names in
    // Drupal configuration (e.g. migration configuration). So we convert them
    // all to underscores.
    $data = $this->makeKeysConfigSafe((array) $item->data());
    // Add the item to the queue.
    $this->queueFactory->get($this->getItemQueueName($item))
      ->createItem($data);
  }

  /**
   * Queue an item for deletion.
   *
   * @param \Drupal\orange_dam\OrangeDamItemInterface $item
   *   The item to delete.
   */
  public function queueDeletedItem(OrangeDamItemInterface $item) {
    $this->queueFactory->get(static::DELETION_QUEUE)
      ->createItem($item);
  }

  /**
   * {@inheritdoc}
   */
  public function runMigrations(
    array $migrationIds = [],
    array $options = [],
    bool $disableDuplicateCheck = FALSE,
  ) {
    // Remove empty options that confuse the migration commands.
    $options = array_filter($options);
    // Capture the state of duplicate checking for this request/process.
    drupal_static(static::DISABLE_DUPLICATE_CHECK_STATE_KEY, $disableDuplicateCheck);
    $results = [];
    // Build a list of migrations to run and loop through them.
    $migrations = $migrationIds ? $this->migrationManager->createInstances($migrationIds) : $this->getMigrations();
    foreach ($migrations as $migration) {
      // Reset migration status.
      if ($migration->getStatus() !== MigrationInterface::STATUS_IDLE) {
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
      }
      $migration->getIdMap()->prepareUpdate();
      // @todo Remove call to setTrackLastImported() when we can require Drupal
      // >= 10.1. setTrackLastImported() was deprecated in 10.1 as migrations
      // will now always keep track of the import time.
      // https://www.drupal.org/node/3282894
      // @phpstan-ignore-next-line
      $migration->setTrackLastImported(TRUE);
      $executable = new MigrateExecutable($migration, NULL, $options);
      $executable->import();
      $results[] = [
        'migration' => $migration->label(),
        'updated' => $executable->getUpdatedCount(),
        'created' => $executable->getCreatedCount(),
        'failed' => $executable->getFailedCount(),
        'ignored' => $executable->getIgnoredCount(),
        'remaining' => $migration->getSourcePlugin()->count(),
      ];
      $this->logger->notice($this->t(
        ':migration - Updated :updated items. Created :created items', [
          ':migration' => $migration->label(),
          ':updated' => $executable->getUpdatedCount(),
          ':created' => $executable->getCreatedCount(),
        ])
      );
    }
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrations(string $migrationTag = self::MIGRATION_TAG): array {
    return $this->migrationManager->createInstancesByTag($migrationTag);
  }

  /**
   * Convenience method for returning only the taxonomy migration IDs.
   *
   * @return array
   *   An array of taxonomy migration IDs.
   */
  public function getTaxonomyMigrationIds(): array {
    $migrations = $this->getMigrations();
    // Filter migrations to just the orange_dam_vocabulary migrations.
    $migrations = array_filter($migrations, fn ($migration) => in_array(static::MIGRATION_TAG_TAXONOMY, $migration->getMigrationTags()));
    $taxonomyMigrationIds = [];
    foreach ($migrations as $migration) {
      $taxonomyMigrationIds[] = $migration->getPluginId();
    }

    return $taxonomyMigrationIds;
  }

  /**
   * Get a list of migration names keyed by the migration machine name.
   *
   * @return string[]
   *   An array of migration labels keyed by the migration machine name.
   */
  public function getMigrationListing(): array {
    $migrations = [];
    foreach ($this->getMigrations() as $migration) {
      $migrations[$migration->id()] = $migration->label();
    }
    return $migrations;
  }

  /**
   * Get the deletion queue.
   *
   * @return \Drupal\Core\Queue\QueueInterface
   *   The deletion queue.
   */
  public function getDeletionQueue(): QueueInterface {
    return $this->queueFactory->get(static::DELETION_QUEUE);
  }

  /**
   * Run the deletion queue.
   *
   * This mimics "drush queue:run".
   *
   * @param int $limit
   *   The maximum number of items to process.
   *
   * @return int
   *   The number of items processed.
   */
  public function processDeletionQueue(int $limit = 0): int {
    $processed = 0;
    $deletionQueue = $this->getDeletionQueue();
    $deletionQueueWorker = $this->queueManager->createInstance(static::DELETION_QUEUE);
    while ($item = $deletionQueue->claimItem(static::DELETION_QUEUE_LEASE_TIME)) {
      // @todo PHPStan is complaining because claimItem() could return FALSE if
      // it is unable to claim an item so $item could be FALSE. But since we
      // cannot be inside this loop if $item is FALSE, then we will never
      // encounter a situation where we request data from FALSE. I can't see any
      // decent means to satisfy PHPStan here. It's an odd omission and maybe
      // should be a bug report. Ignore.
      try {
        // @phpstan-ignore-next-line
        $deletionQueueWorker->processItem($item->data);
        $deletionQueue->deleteItem($item);
      }
      catch (\Exception $e) {
        $this->logger->warning($e->getMessage());
      }
      finally {
        $processed++;
      }
      // Enforce the limit if one was supplied.
      if ($limit && $processed >= $limit) {
        break;
      }
    }
    return $processed;
  }

  /**
   * Check if a migration ID is valid.
   *
   * @param string $migrationId
   *   The migration name.
   *
   * @return bool
   *   Indication if the migration is valid.
   */
  public function validateMigrationId(string $migrationId): bool {
    return in_array($migrationId, array_keys($this->getMigrationListing()));
  }

  /**
   * Determine the name of the queue to add the item to.
   *
   * This matches the format generated by the queue deriver.
   *
   * @param \Drupal\orange_dam\OrangeDamItemInterface $item
   *   The Orange DAM item.
   *
   * @return string
   *   The queue name to add the add item to.
   *
   * @see \Drupal\orange_dam\Plugin\QueueWorker\OrangeDamMigrationQueueWorker
   */
  public function getItemQueueName(OrangeDamItemInterface $item): string {
    return sprintf(
      'orange_dam_migration:%s',
      $this->buildMigrationName($item->type()),
    );
  }

  /**
   * Builds a migration name from an item type.
   *
   * @param string $type
   *   The item type from Orange DAM.
   *
   * @return string
   *   The migration name.
   */
  public function buildMigrationName(string $type): string {
    // Strip all non-alphanumeric characters and replace them with underscores.
    $name = preg_replace('@[^a-z0-9]+@', '_', strtolower($type));
    // Remove duplicate underscores next to each other.
    $name = preg_replace('/_{2,}/', '_', $name);
    // Build the queue name.
    return sprintf('orange_dam_%s', $name);
  }

  /**
   * Get the list of migration queues keyed by queue ID.
   *
   * @return array
   *   The migration queues plugin definitions keyed by queue ID.
   */
  public function getQueues(): array {
    $orangeDamMigrationQueues = [];
    // Show the size of each migration queue.
    $queues = $this->queueManager->getDefinitions();
    foreach ($queues as $id => $queue) {
      if (strpos($id, static::MIGRATION_TAG) !== FALSE) {
        $orangeDamMigrationQueues[$id] = $queue;
      }
    }
    return $orangeDamMigrationQueues;
  }

  /**
   * Validates an Orange DAM queue name.
   *
   * @param string $queueName
   *   The queue machine name.
   *
   * @return bool
   *   Boolean indicating if the queue name is valid.
   */
  public function validateQueueName(string $queueName): bool {
    return in_array($queueName, array_keys($this->getQueues()));
  }

  /**
   * Finds the migration associated with a given item.
   *
   * @param \Drupal\orange_dam\OrangeDamItemInterface $item
   *   The item to find the migration for.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface|false
   *   The migration if one was found, otherwise FALSE.
   */
  public function getItemMigration(OrangeDamItemInterface $item) {
    $queueName = $this->getItemQueueName($item);
    foreach ($this->getMigrations() as $migration) {
      $configuration = $migration->getSourceConfiguration();
      if ($queueName == $configuration['queue_name']) {
        return $migration;
      }
    }
    return FALSE;
  }

  /**
   * Find the migration associated with a given system identifier.
   *
   * @param string $systemIdentifier
   *   The system identifier from Orange DAM.
   * @param string $migrationTag
   *   The migration tag.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface|false
   *   The migration used for the source system identifier.
   */
  public function getMigrationBySourceId(string $systemIdentifier, string $migrationTag = self::MIGRATION_TAG) {
    // Look for the item as a source ID in all the migrations.
    foreach ($this->getMigrations($migrationTag) as $migration) {
      $entityIds = $migration->getIdMap()->lookupDestinationIds([$systemIdentifier]);
      if (!empty($entityIds)) {
        return $migration;
      }
    }
    return FALSE;
  }

  /**
   * Remove periods from array keys, so they can be keys in migration config.
   *
   * @param array $array
   *   The array to filter.
   *
   * @return array
   *   The filtered array.
   */
  protected function makeKeysConfigSafe(array $array): array {
    foreach ($array as $key => $value) {
      $keyNew = str_replace('.', '_', $key);
      if ($key != $keyNew) {
        $array[$keyNew] = $value;
        unset($array[$key]);
      }
    }
    return $array;
  }

}
