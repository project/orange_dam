<?php

/**
 * @file
 * Rector configuration.
 */

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\CompleteDynamicPropertiesRector;
use Rector\CodeQuality\Rector\If_\ExplicitBoolCompareRector;
use Rector\Config\RectorConfig;
use Rector\DeadCode\Rector\ClassMethod\RemoveUselessParamTagRector;
use Rector\DeadCode\Rector\ClassMethod\RemoveUselessReturnTagRector;
use Rector\DeadCode\Rector\Property\RemoveUselessVarTagRector;
use Rector\Php73\Rector\FuncCall\StringifyStrNeedlesRector;
use Rector\Php80\Rector\FunctionLike\MixedTypeRector;
use Rector\Php80\Rector\NotIdentical\StrContainsRector;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;

return static function (RectorConfig $rectorConfig): void {
  $rectorConfig->paths([
    __DIR__ . '/src',
    __DIR__ . '/tests',
  ]);

  // Define sets of rules.
  $rectorConfig->sets([
    LevelSetList::UP_TO_PHP_80,
    SetList::CODE_QUALITY,
    SetList::DEAD_CODE,
  ]);

  $rectorConfig->skip([
    CompleteDynamicPropertiesRector::class,
    ExplicitBoolCompareRector::class,
    MixedTypeRector::class,
    RemoveUselessParamTagRector::class,
    RemoveUselessReturnTagRector::class,
    RemoveUselessVarTagRector::class,
    StrContainsRector::class,
    StringifyStrNeedlesRector::class,
  ]);
};
