<?php

namespace Drupal\orange_dam;

use PHPUnit\Framework\TestCase;

/**
 * OrangeDamKeyword tests.
 */
final class OrangeDamKeywordTest extends TestCase {

  /**
   * Test creation of OrangeDamKeyword class.
   */
  public function testOrangeDamKeywordCreation(): void {
    $keyword = new OrangeDamKeyword('id', (object) [], 'type');
    $this->assertSame('id', $keyword->id());
    $this->assertEquals((object) [
      'orange_dam_item_type' => 'orange_dam_keyword',
      'orange_dam_content_type' => 'type',
    ], $keyword->data());
    $this->assertSame('type', $keyword->type());
  }

}
